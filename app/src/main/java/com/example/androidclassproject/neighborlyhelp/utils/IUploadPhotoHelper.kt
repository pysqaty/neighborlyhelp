package com.example.androidclassproject.neighborlyhelp.utils

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.provider.MediaStore
import android.support.v4.app.Fragment
import com.example.androidclassproject.neighborlyhelp.constants.GALLERY
import com.example.androidclassproject.neighborlyhelp.constants.CAMERA

interface IUploadPhotoHelper {
    fun chooseImageFromGallery(c: Context) {
        val galleryIntent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        if(c is Activity) {
            c.startActivityForResult(galleryIntent, GALLERY)
        } else if(c is Fragment) {
            c.startActivityForResult(galleryIntent, GALLERY)
        }

    }
    fun takePhotoFromCamera(c: Context) {
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if(c is Activity) {
            c.startActivityForResult(cameraIntent, CAMERA)
        } else if(c is Fragment) {
            c.startActivityForResult(cameraIntent, CAMERA)
        }

    }
}