package com.example.androidclassproject.neighborlyhelp.utils

interface IOnGetDataListener {
    fun onSuccess(data : Any?)
    fun onFailure()
}