package com.example.androidclassproject.neighborlyhelp.models

import java.util.*

data class TextMessage(val text: String, val time: Date, val senderId: String){
    constructor() : this("", Date(),"")
}