package com.example.androidclassproject.neighborlyhelp.models

import android.content.Context
import android.util.Log
import android.widget.ImageView
import com.example.androidclassproject.neighborlyhelp.R
import com.example.androidclassproject.neighborlyhelp.glide.GlideApp
import com.example.androidclassproject.neighborlyhelp.utils.FirebaseCommunicator
import com.example.androidclassproject.neighborlyhelp.utils.IOnGetDataListener
import com.google.firebase.firestore.DocumentSnapshot
import com.xwray.groupie.kotlinandroidextensions.Item
import kotlinx.android.synthetic.main.chat_from_row.view.*
import kotlinx.android.synthetic.main.chat_to_row.view.*


class MessageItem(val message: TextMessage,val context:Context) : Item() {
    private val db = FirebaseCommunicator.getInstance()
    override fun bind(viewHolder: com.xwray.groupie.kotlinandroidextensions.ViewHolder, position: Int) {
        if (from) {
            viewHolder.itemView.from_message_text.text = message.text
        } else {
            viewHolder.itemView.to_message_text.text = message.text
        }
    }

    private val from = message.senderId != FirebaseCommunicator.getInstance().getCurrentId()
    override fun getLayout(): Int {
        return if(from)
            R.layout.chat_from_row
        else
            R.layout.chat_to_row
    }

    override fun equals(other: Any?): Boolean {
        return isSameAs(other as? MessageItem)
    }
    override fun hashCode(): Int {
        var result = message.hashCode()
        result = 31 * result + context.hashCode()
        return result
    }

    override fun isSameAs(other: com.xwray.groupie.Item<*>?): Boolean {
        if (other !is MessageItem)
            return false
        if (this.message != other.message)
            return false
        return true
    }

}