package com.example.androidclassproject.neighborlyhelp.fragments

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.Toast

import com.example.androidclassproject.neighborlyhelp.R
import com.example.androidclassproject.neighborlyhelp.activities.MainActivity
import com.example.androidclassproject.neighborlyhelp.utils.CircleImageView
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_add_edit_profile.*
import kotlinx.android.synthetic.main.fragment_login.*

class LoginFragment : Fragment() {
    private var listener: OnFragmentInteractionListener? = null
    private lateinit var mIFragmentManagerActivity: IFragmentManagerActivity


    //widgets
    lateinit var loginBtn : Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.activity_add_edit_profile, container, false)
        view.findViewById<CircleImageView>(R.id.edit_profile_foto).visibility = View.GONE
        view.findViewById<LinearLayout>(R.id.username_box).visibility = View.GONE
        view.findViewById<LinearLayout>(R.id.password_box).visibility = View.VISIBLE
        view.findViewById<LinearLayout>(R.id.name_box).visibility = View.GONE
        view.findViewById<LinearLayout>(R.id.surname_box).visibility = View.GONE
        view.findViewById<LinearLayout>(R.id.phone_box).visibility = View.GONE

        loginBtn = view.findViewById(R.id.save_changes)
        loginBtn.text = getString(R.string.login)
        loginBtn.setOnClickListener{
            if(!loginUser()) return@setOnClickListener
        }
        return view
    }

    fun loginUser() : Boolean {
        val email = edit_mail.text.toString()
        val password = edit_password.text.toString()

        if(email.isEmpty() || password.isEmpty()){
            Toast.makeText(activity, "Email i hasło nie mogą być puste", Toast.LENGTH_SHORT).show()
            return false
        }

        Log.d("LOGIN", "email is: $email")
        Log.d("LOGIN", "password is: $password")


        FirebaseAuth.getInstance().signInWithEmailAndPassword(email, password)
            .addOnCompleteListener{
                if(!it.isSuccessful) return@addOnCompleteListener

                Log.d("LOGIN", "Successfully logged in user with uid: ${it.result?.user?.uid}")
//                mIFragmentManagerActivity.replaceFragment(getString(R.string.fragment_latest_messages))
                var intent = Intent(context, MainActivity::class.java)
                activity!!.finish()
                startActivity(intent)
            }
            .addOnFailureListener{
                Log.d("LOGIN", "Failed to login user: ${it.message}")
            }

        return true
    }
    override fun onAttach(context: Context) {
        super.onAttach(context)
        mIFragmentManagerActivity = activity as IFragmentManagerActivity
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }



    companion object {
        @JvmStatic
        fun newInstance() =
            LoginFragment().apply {
                arguments = Bundle().apply {
                }
            }
    }
}
