package com.example.androidclassproject.neighborlyhelp.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.SparseArray
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.androidclassproject.neighborlyhelp.holders.OfferViewHolder
import com.example.androidclassproject.neighborlyhelp.holders.SectionOfferViewHolder
import com.example.androidclassproject.neighborlyhelp.models.Section
import java.util.*


class SectionedOffersAdapter(
    private val mContext: Context, private val mSectionResourceId: Int, private val mTextResourceId: Int,
    private val mBaseAdapter: OffersAdapter
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var mValid = true
    private val mLayoutInflater: LayoutInflater
    private val mSections = SparseArray<Section>()


    init {

        mLayoutInflater = mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

        mBaseAdapter.registerAdapterDataObserver(object : RecyclerView.AdapterDataObserver() {
            override fun onChanged() {
                mValid = mBaseAdapter.itemCount > 0
                notifyDataSetChanged()
            }

            override fun onItemRangeChanged(positionStart: Int, itemCount: Int) {
                mValid = mBaseAdapter.itemCount > 0
                notifyItemRangeChanged(positionStart, itemCount)
            }

            override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
                mValid = mBaseAdapter.itemCount > 0
                notifyItemRangeInserted(positionStart, itemCount)
            }

            override fun onItemRangeRemoved(positionStart: Int, itemCount: Int) {
                mValid = mBaseAdapter.itemCount > 0
                notifyItemRangeRemoved(positionStart, itemCount)
            }
        })
    }


    override fun onCreateViewHolder(parent: ViewGroup, typeView: Int): RecyclerView.ViewHolder {
        when (typeView) {
            SECTION_TYPE -> {
                val view = LayoutInflater.from(mContext).inflate(mSectionResourceId, parent, false)
                return SectionOfferViewHolder(view, mTextResourceId)
            }
            else -> return mBaseAdapter.onCreateViewHolder(parent, typeView - 1)
        }
    }

    override fun onBindViewHolder(sectionViewHolder: RecyclerView.ViewHolder, position: Int) {
        if (isSectionHeaderPosition(position)) {
            (sectionViewHolder as SectionOfferViewHolder).title.text = mSections.get(position).title
        } else {
            mBaseAdapter.onBindViewHolder(sectionViewHolder as OfferViewHolder, sectionedPositionToPosition(position))
        }

    }

    override fun getItemViewType(position: Int): Int {
        return if (isSectionHeaderPosition(position))
            SECTION_TYPE
        else
            mBaseAdapter.getItemViewType(sectionedPositionToPosition(position)) + 1
    }


    fun setSections(sections: Array<Section>) {
        mSections.clear()

        Arrays.sort(sections
        ) { o, o1 ->
            when {
                o.firstPosition == o1.firstPosition -> 0
                o.firstPosition < o1.firstPosition -> -1
                else -> 1
            }
        }

        for ((offset, section) in sections.withIndex()) {
            section.sectionedPosition = section.firstPosition + offset
            mSections.append(section.sectionedPosition, section)
        }

        notifyDataSetChanged()
    }

    fun positionToSectionedPosition(position: Int): Int {
        var offset = 0
        for (i in 0 until mSections.size()) {
            if (mSections.valueAt(i).firstPosition > position) {
                break
            }
            ++offset
        }
        return position + offset
    }

    private fun sectionedPositionToPosition(sectionedPosition: Int): Int {
        if (isSectionHeaderPosition(sectionedPosition)) {
            return RecyclerView.NO_POSITION
        }

        var offset = 0
        for (i in 0 until mSections.size()) {
            if (mSections.valueAt(i).sectionedPosition > sectionedPosition) {
                break
            }
            --offset
        }
        return sectionedPosition + offset
    }

    fun isSectionHeaderPosition(position: Int): Boolean {
        return mSections.get(position) != null
    }


    override fun getItemId(position: Int): Long {
        return if (isSectionHeaderPosition(position))
            (Integer.MAX_VALUE - mSections.indexOfKey(position)).toLong()
        else
            mBaseAdapter.getItemId(sectionedPositionToPosition(position))
    }

    override fun getItemCount(): Int {
        return if (mValid) {
            mBaseAdapter.itemCount + mSections.size()
        } else 0
    }

    companion object {
        private const val SECTION_TYPE = 0
    }

}