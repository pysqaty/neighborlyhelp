package com.example.androidclassproject.neighborlyhelp.models

import android.graphics.Bitmap
import com.example.androidclassproject.neighborlyhelp.enums.OfferCategory
import java.util.*


class Offer(
    var id: String = "",
    var description: String = "",
    var issuer: String = "",
    var executor: String = "",
    var contacts: MutableList<String> = mutableListOf(),
    var accepted: MutableList<String> = mutableListOf(),
    var isEnded: Boolean = false,
    var location: LatLng = LatLng(0.0, 0.0),
    var address: String = "",
    var cost: Double = 0.0,
    var dateFrom: Date = Date(),
    var dateTo: Date = Date(),
    var createdAt: Date = Date(),
    var category: String = "",
    var photoPath: String? = null,
    var wasRatedByIssuer: Boolean = false,
    var wasRatedByExecutor: Boolean = false
)
