package com.example.androidclassproject.neighborlyhelp.models

class User(
    var uid: String,
    var nickname: String,
    var email: String
) {
    fun getProfileData(): Profile? {
        return Profile(uid, nickname, "", "", email, "", 0.0, 0, null)
    }

    constructor() : this ("", "", "")
}
