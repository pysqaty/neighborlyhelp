package com.example.androidclassproject.neighborlyhelp.models

class Section(internal var firstPosition: Int, title: CharSequence) {
    internal var sectionedPosition: Int = 0
    var title: CharSequence
        internal set

    init {
        this.title = title
    }
}