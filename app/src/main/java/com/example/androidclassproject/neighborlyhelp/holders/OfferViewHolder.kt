package com.example.androidclassproject.neighborlyhelp.holders

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.graphics.Bitmap
import android.os.Debug
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.*
import com.example.androidclassproject.neighborlyhelp.R
import com.example.androidclassproject.neighborlyhelp.activities.MainActivity
import com.example.androidclassproject.neighborlyhelp.adapters.ContactsAdapter
import com.example.androidclassproject.neighborlyhelp.models.Offer
import com.example.androidclassproject.neighborlyhelp.models.Profile
import com.example.androidclassproject.neighborlyhelp.utils.FirebaseCommunicator
import com.example.androidclassproject.neighborlyhelp.utils.IContracter
import com.example.androidclassproject.neighborlyhelp.utils.IOnGetDataListener
import com.example.androidclassproject.neighborlyhelp.utils.IRefreshable
import com.google.firebase.firestore.DocumentSnapshot
import kotlinx.android.synthetic.main.expanded_offer_list_item.view.*
import kotlinx.android.synthetic.main.fragment_offers.view.*
import java.text.SimpleDateFormat

class OfferViewHolder(val view: View, val refreshable: IRefreshable) :  RecyclerView.ViewHolder(view), IContracter {
    private var offerDescription = view.findViewById<TextView>(R.id.offer_description)
    private var dateFrom = view.findViewById<TextView>(R.id.date_from)
    private var dateTo = view.findViewById<TextView>(R.id.date_to)
    private var offerCost = view.findViewById<TextView>(R.id.offer_cost)
    private var offerImage = view.findViewById<ImageView>(R.id.offer_image)
    private var wholeItem = view.findViewById<LinearLayout>(R.id.whole_offer_item)
    private lateinit var contactRequestButton: Button
    private lateinit var endOfferButton: Button
    private lateinit var asksToContactRecyclerView: RecyclerView
    private lateinit var rateButton: Button
    private lateinit var ratingPanel: LinearLayout
    private lateinit var rating: RatingBar
    private lateinit var fragmentView: View
    private lateinit var adapter: ContactsAdapter
    private lateinit var currentOffer: Offer
    private lateinit var issuerTextView: TextView
    private lateinit var executorTextView: TextView
    private var firebaseCommunicator = FirebaseCommunicator.getInstance()

    private var currentOfferId : String = ""

    fun bind(offer: Offer, fragmentView: View, myId: String) {


        currentOffer = offer
        this@OfferViewHolder.fragmentView = fragmentView
        offerDescription.text = currentOffer.description
        val formatter = SimpleDateFormat("dd.MM.yyy")
        dateFrom.text = formatter.format(currentOffer.dateFrom)
        dateTo.text = formatter.format(currentOffer.dateTo)
        offerCost.text = currentOffer.cost.toString() + " zł"
        if (!currentOffer.photoPath.isNullOrEmpty()) {
            firebaseCommunicator.getImage(currentOffer.photoPath!!) { bmp: Bitmap -> offerImage.setImageBitmap(bmp) }
        } else {
            fragmentView.expanded_item_offer_photo.setImageResource(R.drawable.offer_picture_placeholder)
        }

        executorTextView = fragmentView.findViewById(R.id.text_waiting_for_issuer_rating)
        issuerTextView = fragmentView.findViewById(R.id.text_waiting_for_executor_rating)

        contactRequestButton = fragmentView.findViewById(R.id.contact_request_button)

        endOfferButton = fragmentView.findViewById(R.id.end_offer_button)

        asksToContactRecyclerView = fragmentView.findViewById(R.id.asks_to_contact_recycler_view)
        rateButton = fragmentView.findViewById(R.id.rate_user_button)
        ratingPanel = fragmentView.findViewById(R.id.rating_panel)
        rating = fragmentView.findViewById(R.id.rating_bar)





        adapter = ContactsAdapter(this@OfferViewHolder, myId)

        val animationDuration: Long = 700
        wholeItem.setOnClickListener { prepareAndShowExpandElement(myId, formatter, animationDuration)}

        fragmentView.collapse_item_button.setOnClickListener { hideExpandedElement(animationDuration) }
    }

    private fun prepareAndShowExpandElement(myId: String, formatter: SimpleDateFormat, animationDuration: Long) {
        firebaseCommunicator.getOfferById(currentOffer.id, object : IOnGetDataListener {
            override fun onSuccess(data: Any?) {
                currentOffer = (data as DocumentSnapshot).toObject(Offer::class.java)!!
                currentOfferId = currentOffer.id

                rateButton.setOnClickListener(null)
                contactRequestButton.setOnClickListener(null)
                endOfferButton.setOnClickListener(null)

                rateButton.setOnClickListener { rateUserClick(myId) }
                endOfferButton.setOnClickListener {
                    endOfferClick(myId)
                }
                contactRequestButton.setOnClickListener {
                    contactRequestClick(myId)
                }

                setAppropriateButtonVisible(myId)
                val dbListener = object : IOnGetDataListener {
                    override fun onSuccess(data: Any?) {
                        val profile = (data as DocumentSnapshot).toObject(Profile::class.java)
                        fragmentView.expanded_item_issuer_nick.text = profile!!.nickname
                        fragmentView.expanded_item_issuer_nick.setOnClickListener { showProfile(currentOffer.issuer, myId) }
                    }

                    override fun onFailure() {
                        Toast.makeText(wholeItem.context, "Pobieranie profilu nie powiodło się", Toast.LENGTH_SHORT).show()
                    }
                }
                firebaseCommunicator.getProfile(currentOffer.issuer, dbListener)

                fragmentView.expanded_item_cost.text = currentOffer.cost.toString() + " zł"
                fragmentView.expanded_item_created_at.text = formatter.format(currentOffer.createdAt)
                fragmentView.expanded_item_date_from.text = formatter.format(currentOffer.dateFrom)
                fragmentView.expanded_item_date_to.text = formatter.format(currentOffer.dateTo)
                fragmentView.expanded_item_description.text = currentOffer.description
                if (!currentOffer.photoPath.isNullOrEmpty()) {
                    firebaseCommunicator.getImage(currentOffer.photoPath!!) { bmp: Bitmap ->
                        fragmentView.expanded_item_offer_photo.setImageBitmap(
                            bmp
                        )
                    }
                } else {
                    fragmentView.expanded_item_offer_photo.setImageResource(R.drawable.offer_picture_placeholder)
                }
                showExpandedElement(animationDuration)
            }

            override fun onFailure() {
                Toast.makeText(wholeItem.context, "Pobieranie oferty nie powiodło się", Toast.LENGTH_SHORT).show()
            }

        })
    }

    private fun hideExpandedElement(animationDuration: Long = 700) {
        fragmentView.offers_recycler_view.apply {
            alpha = 0f
            visibility = View.VISIBLE

            animate()
                .alpha(1f)
                .setDuration(animationDuration)
                .setListener(null)
        }
        fragmentView.expanded_item.animate()
            .alpha(0f)
            .setDuration(animationDuration)
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    fragmentView.expanded_item.visibility = View.GONE
                }
            })
    }

    private fun showExpandedElement(animationDuration: Long = 700) {
        fragmentView.expanded_item.apply {
            alpha = 0f
            visibility = View.VISIBLE

            animate()
                .alpha(1f)
                .setDuration(animationDuration)
                .setListener(null)
        }

        fragmentView.offers_recycler_view.animate()
            .alpha(0f)
            .setDuration(animationDuration)
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    fragmentView.offers_recycler_view.visibility = View.GONE
                }
            })
    }



    override fun showProfile(id: String, myId: String) {
        val activity = fragmentView.context as MainActivity
        var shouldAll = false
        if(currentOffer.issuer == myId || currentOffer.accepted.contains(myId))
            shouldAll = true
        activity.displayProfile(id, shouldAll)
    }

    private fun setAppropriateButtonVisible(myId: String) {
        var contactV = View.GONE
        var endV = View.GONE
        var asksToContactV = View.GONE
        var ratingPanelV = View.GONE
        var executorTextV = View.GONE
        var issuerTextV = View.GONE

        if(currentOffer.isEnded) {
            if(myId == currentOffer.issuer && currentOffer.wasRatedByIssuer) {
                issuerTextV = View.VISIBLE
            }
            else {

                if (myId == currentOffer.executor && currentOffer.wasRatedByExecutor) {
                    executorTextV = View.VISIBLE
                } else {
                    ratingPanelV = View.VISIBLE
                }
            }
        }
        else {
            if(currentOffer.executor == myId || (currentOffer.issuer == myId && !currentOffer.executor.isNullOrEmpty())) {
                endV = View.VISIBLE
            }

            if(currentOffer.issuer != myId && currentOffer.executor != myId && !currentOffer.contacts.contains(myId) && !currentOffer.accepted.contains(myId)) {
                contactV = View.VISIBLE
            }

            if(currentOffer.issuer == myId && (!currentOffer.contacts.isEmpty() || !currentOffer.accepted.isEmpty()) && currentOffer.executor.isNullOrEmpty()) {
                asksToContactV = View.VISIBLE
            }
        }

        if(asksToContactV == View.VISIBLE) {
            val tmpList = mutableListOf<Pair<String, String>>()
            for(c in currentOffer.contacts) {
                val dbListener = object : IOnGetDataListener {
                    override fun onSuccess(data: Any?) {
                        val profile = (data as DocumentSnapshot).toObject(Profile::class.java)
                        tmpList.add(Pair(profile!!.nickname, c))
                        val layoutManager = LinearLayoutManager(fragmentView.context)
                        asksToContactRecyclerView.layoutManager = layoutManager
                        adapter.swapData(tmpList)
                        asksToContactRecyclerView.adapter = adapter
                        adapter.notifyDataSetChanged()
                    }

                    override fun onFailure() {
                        Toast.makeText(wholeItem.context,"Pobieranie profilu nie powiodło się", Toast.LENGTH_SHORT).show()
                    }
                }
                firebaseCommunicator.getProfile(c, dbListener)
            }
        }

        contactRequestButton.visibility = contactV
        endOfferButton.visibility = endV
        asksToContactRecyclerView.visibility = asksToContactV
        ratingPanel.visibility = ratingPanelV
        executorTextView.visibility = executorTextV
        issuerTextView.visibility = issuerTextV
    }

    private fun contactRequestClick(myId: String) {
        currentOffer.contacts.add(myId)
        firebaseCommunicator.saveOffer(currentOffer, object: IOnGetDataListener {
            override fun onSuccess(data: Any?) {
                hideExpandedElement()
                refreshable.Refresh()
            }

            override fun onFailure() {
            }
        })
    }


    private fun endOfferClick(myId: String) {
        currentOffer.isEnded = true
        if(currentOffer.issuer == myId) {
            firebaseCommunicator.deleteChatChannel(currentOffer.executor, null)
        } else {
            firebaseCommunicator.deleteChatChannel(currentOffer.issuer, null)
        }

        firebaseCommunicator.saveOffer(currentOffer, object: IOnGetDataListener {
            override fun onSuccess(data: Any?) {
                endOfferButton.visibility = View.GONE
                rating.rating = 0.0f
                ratingPanel.visibility = View.VISIBLE
            }

            override fun onFailure() {
            }
        })
    }

    private fun rateUserClick(myId: String) {
        if (currentOffer.issuer.isNullOrEmpty() || currentOffer.executor.isNullOrEmpty())
            Toast.makeText(
                wholeItem.context,
                "Wystąpił błąd przy nadawaniu oceny",
                Toast.LENGTH_SHORT
            ).show()
        else {
            val dbListener = object : IOnGetDataListener {
                override fun onSuccess(data: Any?) {
                    if (myId == currentOffer.issuer)
                        currentOffer.wasRatedByIssuer = true
                    else
                        currentOffer.wasRatedByExecutor = true

                    firebaseCommunicator.saveOffer(currentOffer, object : IOnGetDataListener {
                        override fun onSuccess(data: Any?) {
                            hideExpandedElement()
                            refreshable.Refresh()
                        }

                        override fun onFailure() {
                        }
                    })
                }

                override fun onFailure() {
                }
            }

            if (myId == currentOffer.issuer)
                firebaseCommunicator.rateProfile(currentOffer.executor, rating.rating.toDouble(), dbListener)
            else
                firebaseCommunicator.rateProfile(currentOffer.issuer, rating.rating.toDouble(), dbListener)

        }
    }

    override fun contact(id: String) {
        currentOffer.contacts.remove(id)
        currentOffer.accepted.add(id)
        firebaseCommunicator.getOrCreateChatChannel(id){}
        firebaseCommunicator.saveOffer(currentOffer, object: IOnGetDataListener {
            override fun onSuccess(data: Any?) {
                hideExpandedElement()
                refreshable.Refresh()
            }

            override fun onFailure() {
            }
        })
    }

    override fun accept(id: String) {
        currentOffer.accepted.remove(id)
        for(acceptedId in currentOffer.accepted) {
            firebaseCommunicator.deleteChatChannel(acceptedId, object: IOnGetDataListener {
                override fun onSuccess(data: Any?) {
                    currentOffer.accepted.remove(acceptedId)
                }

                override fun onFailure() {

                }
            })
        }
//        currentOffer.accepted.clear()
        currentOffer.contacts.clear()
        currentOffer.executor = id
        firebaseCommunicator.getOrCreateChatChannel(id){}
        firebaseCommunicator.saveOffer(currentOffer, object: IOnGetDataListener {
            override fun onSuccess(data: Any?) {
                hideExpandedElement()
                refreshable.Refresh()
            }

            override fun onFailure() {
            }
        })
    }

}