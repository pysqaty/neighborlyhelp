package com.example.androidclassproject.neighborlyhelp.models



class LatLng(var latitude: Double = 0.0, var longitude: Double = 0.0) {

    fun toGoogleLatLng() : com.google.android.gms.maps.model.LatLng {
        return com.google.android.gms.maps.model.LatLng(latitude, longitude)
    }
    companion object {
        @JvmStatic
        fun fromGoogleLatLng(ll : com.google.android.gms.maps.model.LatLng) : LatLng {
            return LatLng(ll.latitude, ll.longitude)
        }
    }

}