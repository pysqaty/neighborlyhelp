package com.example.androidclassproject.neighborlyhelp.models

import android.os.Parcel
import android.os.Parcelable

class Profile (
    var id: String,
    var nickname: String,
    var firstName: String?,
    var lastName: String?,
    var email: String?,
    var phoneNumber: String?,
    var rating: Double,
    var ratesCounter: Int,
    var photoPath: String?
){
    constructor():this("","","","","","",0.0, 0,"")
    fun getEditProfileData() : EditProfileData {
        return EditProfileData(nickname, firstName, lastName, email, phoneNumber, photoPath)
    }

    fun setEditProfileData(data : EditProfileData) {
        nickname = data.nickname
        firstName = data.firstName
        lastName = data.lastName
        email = data.email
        phoneNumber = data.phoneNumber
        photoPath = data.photoPath
    }
}