package com.example.androidclassproject.neighborlyhelp.activities

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.provider.MediaStore
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.PopupMenu
import android.widget.Toast
import com.example.androidclassproject.neighborlyhelp.R
import com.example.androidclassproject.neighborlyhelp.constants.GALLERY
import com.example.androidclassproject.neighborlyhelp.constants.CAMERA
import com.example.androidclassproject.neighborlyhelp.fragments.ProfileFragment
import com.example.androidclassproject.neighborlyhelp.models.EditProfileData
import com.example.androidclassproject.neighborlyhelp.utils.*
import java.io.IOException

private const val PHOTO_PATH = "bitmap.png"

class AddEditProfile : IUploadPhotoHelper, AppCompatActivity() {
    private var profileFoto : CircleImageView? = null
    private var profileData : EditProfileData? = null
    private var profileFotoChanged : Boolean = false;
    private lateinit var fbCommunicator: FirebaseCommunicator

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fbCommunicator = FirebaseCommunicator.getInstance()
        setContentView(R.layout.activity_add_edit_profile)
        profileFoto = findViewById(R.id.edit_profile_foto)
        profileFoto?.setOnClickListener{profileFotoClick(profileFoto)}
        profileData = intent?.extras?.get(ProfileFragment.PROFILE_DATA_PARAM) as EditProfileData
        findViewById<Button>(R.id.save_changes).setOnClickListener{saveChangesAndReturn()}
        complementProfileData()
    }

    override fun onBackPressed() {
        setResult(Activity.RESULT_CANCELED)
        super.onBackPressed()
    }

    private fun saveChangesAndReturn() {
        if(!validateFields())
            return

        profileData!!.nickname = findViewById<EditText>(R.id.edit_nick_name).text.toString()
        profileData!!.firstName = findViewById<EditText>(R.id.edit_name).text.toString()
        profileData!!.lastName = findViewById<EditText>(R.id.edit_surname).text.toString()
        profileData!!.phoneNumber = findViewById<EditText>(R.id.edit_phone).text.toString()
        profileData!!.email = findViewById<EditText>(R.id.edit_mail).text.toString()

        val bmpDrawable = findViewById<CircleImageView>(R.id.edit_profile_foto).drawable
        if(bmpDrawable is BitmapDrawable) {
            val bmp = bmpDrawable.bitmap

            if(profileFotoChanged) {
                fbCommunicator.uploadProfilePicture(bmp) { path: String ->
                    profileData!!.photoPath = path
                    Return()
                }
            }
            else {
                Return()
            }
        }
    }

    private fun Return() {
        val resultData = Intent()
        resultData.putExtra(ProfileFragment.PROFILE_DATA_PARAM, profileData)
        setResult(Activity.RESULT_OK, resultData)
        finish()
    }

    private fun validateFields() : Boolean {
        var isAllValid = true
        val nickName = findViewById<EditText>(R.id.edit_nick_name)
        val phoneNumber = findViewById<EditText>(R.id.edit_phone)
        val mail = findViewById<EditText>(R.id.edit_mail)
        isAllValid = isAllValid && checkIfNotEmpty(nickName)
        isAllValid = isAllValid && checkIfIsUnique(nickName)
        isAllValid = isAllValid && checkIfNotEmpty(findViewById(R.id.edit_name))
        isAllValid = isAllValid && checkIfNotEmpty(findViewById(R.id.edit_surname))
        isAllValid = isAllValid && checkIfNotEmpty(phoneNumber)
        isAllValid = isAllValid && checkIfOnlyDigits(phoneNumber)
        isAllValid = isAllValid && checkIfNotEmpty(mail)
        isAllValid = isAllValid && checkIfValidMail(mail)

        return isAllValid
    }



    private fun complementProfileData() {
        findViewById<EditText>(R.id.edit_nick_name).setText(profileData!!.nickname)
        findViewById<EditText>(R.id.edit_name).setText(profileData!!.firstName)
        findViewById<EditText>(R.id.edit_surname).setText(profileData!!.lastName)
        findViewById<EditText>(R.id.edit_mail).setText(profileData!!.email)
        findViewById<EditText>(R.id.edit_phone).setText(profileData!!.phoneNumber)


        if(!profileData!!.photoPath!!.isEmpty()) {
            fbCommunicator.getImage(profileData!!.photoPath!!) { bmp : Bitmap ->  findViewById<CircleImageView>(R.id.edit_profile_foto).setImageBitmap(bmp)}
        }
    }

    private fun profileFotoClick(v : View?) {
        var popup : PopupMenu?
        popup = PopupMenu(v?.context, v)
        popup.inflate(R.menu.add_profile_photo_menu)
        popup.setOnMenuItemClickListener(PopupMenu.OnMenuItemClickListener {
                item: MenuItem? -> when (item!!.itemId) {
            R.id.add_photo_from_gallery -> {chooseImageFromGallery(this)}
            R.id.make_new_photo -> {takePhotoFromCamera(this)}
        }
            true
        })

        popup.show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == GALLERY)
        {
            if (data != null)
            {
                val contentURI = data.data
                try {
                    val bitmap = MediaStore.Images.Media.getBitmap(contentResolver, contentURI)
                    //save to database
                    profileFoto?.setImageBitmap(bitmap)
                    profileFotoChanged = true
                }
                catch (e: IOException)
                {
                    e.printStackTrace()
                }
            }
        }
        else if (requestCode == CAMERA)
        {
            val thumbnail = data!!.extras!!.get("data") as Bitmap
            //save to database
            profileFoto?.setImageBitmap(thumbnail)
            profileFotoChanged = true
        }
    }


    companion object {
        @JvmStatic
        fun newInstance() = AddEditProfile()
    }
}
