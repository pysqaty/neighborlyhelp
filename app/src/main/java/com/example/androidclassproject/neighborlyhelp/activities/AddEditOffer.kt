package com.example.androidclassproject.neighborlyhelp.activities

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.provider.MediaStore
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import android.widget.*
import com.example.androidclassproject.neighborlyhelp.R
import com.example.androidclassproject.neighborlyhelp.enums.OfferCategory
import com.example.androidclassproject.neighborlyhelp.models.Offer
import com.example.androidclassproject.neighborlyhelp.utils.FirebaseCommunicator
import com.example.androidclassproject.neighborlyhelp.utils.checkIfNotEmpty
import com.google.android.gms.common.GooglePlayServicesNotAvailableException
import com.google.android.gms.common.GooglePlayServicesRepairableException
import com.google.android.gms.location.places.ui.PlacePicker
import java.io.IOException
import java.util.*
import com.example.androidclassproject.neighborlyhelp.constants.GALLERY
import com.example.androidclassproject.neighborlyhelp.constants.CAMERA
import com.example.androidclassproject.neighborlyhelp.utils.IUploadPhotoHelper
import kotlinx.android.synthetic.main.activity_add_edit_offer.*

const val PLACE_PICKER_REQUEST = 3

class AddEditOffer :AdapterView.OnItemSelectedListener,
    IUploadPhotoHelper, AppCompatActivity() {

    private val firebaseCommunicator: FirebaseCommunicator = FirebaseCommunicator.getInstance()
    var offerData: Offer = Offer()
    private var offerPhotoChanged : Boolean = false


    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        offerData.category = parent?.selectedItem.toString()
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_edit_offer)
        offer_photo.setOnClickListener { offerPhotoClick(offer_photo) }
        save_changes.setOnClickListener{ saveChangesAndReturn() }
        offer_date_from.setOnClickListener { setDateFromDatePicker(offer_date_from, true) }
        offer_date_to.setOnClickListener { setDateFromDatePicker(offer_date_to, false) }
        offer_location.setOnClickListener {
            val builder = PlacePicker.IntentBuilder()
            try {
                startActivityForResult(builder.build(this), PLACE_PICKER_REQUEST)
            } catch (e: GooglePlayServicesRepairableException) {
                e.printStackTrace()
            } catch (e: GooglePlayServicesNotAvailableException) {
                e.printStackTrace()
            }
        }

        firebaseCommunicator.getCategories { categories ->
            offer_category.onItemSelectedListener = this
            ArrayAdapter(this, android.R.layout.simple_spinner_item, categories)
                .also { adapter ->
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                    offer_category.adapter = adapter
                }
        }

        offerData.issuer = firebaseCommunicator.getCurrentId()
    }

    private fun saveChangesAndReturn() {
        if(!validateFields())
            return

        offerData.description = offer_description.text.toString()
        offerData.cost = offer_cost.text.toString().toDouble()
        offerData.createdAt = Calendar.getInstance().time

        val bmpDrawable = offer_photo.drawable
        if(bmpDrawable is BitmapDrawable) {
            val bmp = bmpDrawable.bitmap

            if(offerPhotoChanged) {
                firebaseCommunicator.uploadProfilePicture(bmp) { path: String ->
                    offerData.photoPath = path
                    finishActivity()
                }
            }
            else {
                finishActivity()
            }
        }
        else {
            finishActivity()
        }

    }

    private fun finishActivity() {
        firebaseCommunicator.saveOffer(offerData, null)
        val resultData = Intent()
        setResult(Activity.RESULT_OK, resultData)
        finish()
    }

    private fun validateFields() : Boolean {
        var isAllValid = true
        isAllValid = isAllValid && checkIfNotEmpty(findViewById<EditText>(R.id.offer_description))
        isAllValid = isAllValid && checkIfNotEmpty(findViewById<EditText>(R.id.offer_cost))
        isAllValid = isAllValid && checkIfNotEmpty(findViewById<TextView>(R.id.offer_date_from))
        isAllValid = isAllValid && checkIfNotEmpty(findViewById<TextView>(R.id.offer_date_to))
        isAllValid = isAllValid && checkIfNotEmpty(findViewById<TextView>(R.id.offer_location))
        return isAllValid
    }

    private fun offerPhotoClick(v : View?) {
        val popup : PopupMenu?
        popup = PopupMenu(v?.context, v)
        popup.inflate(R.menu.add_profile_photo_menu)
        popup.setOnMenuItemClickListener(PopupMenu.OnMenuItemClickListener {
                item: MenuItem? -> when (item!!.itemId) {
            R.id.add_photo_from_gallery -> {chooseImageFromGallery(this)}
            R.id.make_new_photo -> {takePhotoFromCamera(this)}
        }
            true
        })

        popup.show()
    }

    private fun setDateFromDatePicker(v: TextView, isFrom: Boolean) {
        val cldr = Calendar.getInstance()
        val day = cldr.get(Calendar.DAY_OF_MONTH)
        val month = cldr.get(Calendar.MONTH)
        val year = cldr.get(Calendar.YEAR)
        val picker = DatePickerDialog(this,
            DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
                v.text = dayOfMonth.toString() + "/" + (monthOfYear + 1) + "/" + year
                val c = Calendar.getInstance()
                c.set(year, monthOfYear, dayOfMonth)
                if(isFrom) {
                    offerData.dateFrom = c.time
                }
                else {
                    offerData.dateTo = c.time
                }
            },
            year,
            month,
            day
        )
        picker.show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when(requestCode) {
                GALLERY -> {
                    if (data != null)
                    {
                        val contentURI = data.data
                        try {
                            val bitmap = MediaStore.Images.Media.getBitmap(contentResolver, contentURI)
                            //save to database
                            offer_photo?.setImageBitmap(bitmap)
                            offerPhotoChanged = true
                        }
                        catch (e: IOException)
                        {
                            e.printStackTrace()
                        }
                    }
                }
                CAMERA -> {
                    val thumbnail = data!!.extras!!.get("data") as Bitmap
                    offer_photo?.setImageBitmap(thumbnail)
                    offerPhotoChanged = true
                }
                PLACE_PICKER_REQUEST -> {
                    val selectedPlace = PlacePicker.getPlace(this, data)
                    offer_location.text = selectedPlace.address
                    offerData.address = selectedPlace.address.toString()
                    offerData.location = com.example.androidclassproject.neighborlyhelp.models.LatLng.fromGoogleLatLng(selectedPlace.latLng)
                }
            }
        }
    }
}
