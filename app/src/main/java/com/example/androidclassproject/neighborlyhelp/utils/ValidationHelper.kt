package com.example.androidclassproject.neighborlyhelp.utils

import android.widget.EditText
import android.widget.TextView

fun checkIfValidMail(form : EditText) : Boolean {
    form.error = null
    val reg = Regex("^[0-9A-Za-z]+@[0-9A-Za-z]+\\.[0-9A-Za-z]{1,3}\$")
    if(!reg.matches(form.text)) {
        form.error = "Niepoprawny mail"
        return false
    }
    return true
}

fun checkIfOnlyDigits(form : EditText) : Boolean {
    form.error = null
    val reg = Regex("^[0-9]+$")
    if(!reg.matches(form.text)) {
        form.error = "Tylko liczby"
        return false
    }
    return true
}

fun checkIfIsUnique(form : EditText) : Boolean {
    //check if nickname is unique
    return true
}

fun checkIfNotEmpty(form : EditText) : Boolean {
    form.error = null
    if(form.text.isEmpty()) {
        form.error = "Pole nie może być puste"
        return false
    }
    return true
}

fun checkIfNotEmpty(form : TextView) : Boolean {
    form.error = null
    if(form.text.isEmpty()) {
        form.error = "Pole nie może być puste"
        return false
    }
    return true
}