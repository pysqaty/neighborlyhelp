package com.example.androidclassproject.neighborlyhelp.fragments

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.PopupMenu
import android.widget.Toast
import com.example.androidclassproject.neighborlyhelp.R
import com.example.androidclassproject.neighborlyhelp.activities.MainActivity
import com.example.androidclassproject.neighborlyhelp.models.Profile
import com.example.androidclassproject.neighborlyhelp.utils.CircleImageView
import com.example.androidclassproject.neighborlyhelp.utils.FirebaseCommunicator
import com.example.androidclassproject.neighborlyhelp.utils.IDBCommunicator
import com.example.androidclassproject.neighborlyhelp.utils.IOnGetDataListener
import kotlinx.android.synthetic.main.activity_add_edit_profile.*
import org.jetbrains.anko.imageBitmap
import java.io.IOException
import com.example.androidclassproject.neighborlyhelp.utils.IUploadPhotoHelper
import com.example.androidclassproject.neighborlyhelp.constants.GALLERY
import com.example.androidclassproject.neighborlyhelp.constants.CAMERA


class RegisterUserFragment : Fragment(), IUploadPhotoHelper {

    private lateinit var mIFragmentManagerActivity: IFragmentManagerActivity
    private lateinit var db : IDBCommunicator

    //widgets
    lateinit var registerBtn : Button


    private var mListener: OnFragmentInteractionListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
        }


    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment

        val view = inflater.inflate(R.layout.activity_add_edit_profile, container, false)
        view.findViewById<LinearLayout>(R.id.password_box).visibility = View.VISIBLE
        view.findViewById<CircleImageView>(R.id.edit_profile_foto).visibility = View.GONE

        registerBtn = view.findViewById(R.id.save_changes)
        registerBtn.text = getString(R.string.create_account)
        registerBtn.setOnClickListener{
            if(!registerUser()) return@setOnClickListener
        }
        return view
    }

    fun registerUser() : Boolean{
        val nickname = edit_nick_name.text.toString()
        val firstName = edit_name.text.toString()
        val lastName = edit_surname.text.toString()
        val email = edit_mail.text.toString()
        val phone = edit_phone.text.toString()


        val password = edit_password.text.toString()


        if(email.isEmpty() || password.isEmpty()){
            Toast.makeText(activity, "Email i hasło nie mogą być puste", Toast.LENGTH_SHORT).show()
            return false
        }
        Log.d("REGISTER", "email is: $email")
        Log.d("REGISTER", "password is: $password")

        // TODO: Get the fields from the form and create an actual profile
        val user = Profile("",nickname,firstName,lastName,email,phone,0.0,0, "")

        val listener = object : IOnGetDataListener {
            override fun onSuccess(data: Any?) {
                val intent = Intent(context, MainActivity::class.java)
                activity!!.finish()
                startActivity(intent)
            }

            override fun onFailure() {
                Toast.makeText(context,"Rejestracja nie powiodła się", Toast.LENGTH_SHORT).show()
            }
        }
        db.registerProfile(user,password, listener)


        return true
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        mIFragmentManagerActivity = activity as IFragmentManagerActivity
        db = FirebaseCommunicator.getInstance()
        if (context is OnFragmentInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException("$context must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    companion object {
        fun newInstance(): RegisterUserFragment {
            val fragment = RegisterUserFragment()
            val args = Bundle()
            fragment.arguments = args
            return fragment
        }
    }
}
