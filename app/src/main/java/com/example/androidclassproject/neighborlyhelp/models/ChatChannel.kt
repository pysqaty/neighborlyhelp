package com.example.androidclassproject.neighborlyhelp.models

data class ChatChannel(val userId: MutableList<String>){
    constructor():this(mutableListOf())
}