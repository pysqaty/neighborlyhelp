package com.example.androidclassproject.neighborlyhelp.models

import android.content.Context
import com.example.androidclassproject.neighborlyhelp.R
import com.example.androidclassproject.neighborlyhelp.glide.GlideApp
import com.example.androidclassproject.neighborlyhelp.utils.FirebaseCommunicator
import com.xwray.groupie.kotlinandroidextensions.Item
import kotlinx.android.synthetic.main.chat_row_latest_message.view.*

class ChatItem(val user : Profile, private val context: Context) :  Item(){
    private val db = FirebaseCommunicator.getInstance()
    override fun bind(viewHolder: com.xwray.groupie.kotlinandroidextensions.ViewHolder, position: Int) {
        viewHolder.itemView.username_textview_recentmessages.text = user.nickname
        if(user.photoPath != "")
            GlideApp.with(context)
                .load(db.pathToReference(user.photoPath!!))
                .placeholder(R.drawable.person_icon)
                .into(viewHolder.itemView.chat_row_image)
    }

    override fun getLayout(): Int {
        return R.layout.chat_row_latest_message
    }
}