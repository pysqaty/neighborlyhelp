package com.example.androidclassproject.neighborlyhelp.fragments

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.androidclassproject.neighborlyhelp.R

private const val PROFILE_ID_PARAM = "PROFILE_ID"
class OffersFragment : Fragment() {
    private var listener: OnFragmentInteractionListener? = null
    private var myId: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            myId = it.getString(PROFILE_ID_PARAM)
        }
        val offersFragment = OffersListsFragment.newInstance(myId!!)
        val transaction = childFragmentManager.beginTransaction()
        transaction.add(R.id.offers_fragments_container, offersFragment).commit()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_offers_lists, container, false)
    }



    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    fun openFilterFragment() {
        val offersFilterFragment = OffersFilterFragment.newInstance(myId!!)
        val transaction = childFragmentManager.beginTransaction()
        transaction.replace(R.id.offers_fragments_container, offersFilterFragment).addToBackStack(null).commit()
    }


    companion object {
        @JvmStatic
        fun newInstance(myId: String) =
            OffersFragment().apply {
                arguments = Bundle().apply {
                    putString(PROFILE_ID_PARAM, myId)
                }
            }
    }
}
