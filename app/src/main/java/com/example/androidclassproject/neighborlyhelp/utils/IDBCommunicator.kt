package com.example.androidclassproject.neighborlyhelp.utils
import android.content.Context
import android.graphics.Bitmap
import com.example.androidclassproject.neighborlyhelp.models.Offer
import com.example.androidclassproject.neighborlyhelp.models.Profile
import com.example.androidclassproject.neighborlyhelp.models.TextMessage
import com.example.androidclassproject.neighborlyhelp.models.User
import com.google.firebase.firestore.GeoPoint
import com.google.firebase.firestore.ListenerRegistration
import com.google.firebase.firestore.local.ReferenceSet
import com.google.firebase.storage.StorageReference
import com.xwray.groupie.kotlinandroidextensions.Item

interface IDBCommunicator {
    /**
     * Offers Functions:
     */
    fun saveOffer(offer : Offer, listener: IOnGetDataListener?)
    fun getUserOffers(uid : String, listener : IOnGetDataListener?)
    fun getAllOffers(listener: IOnGetDataListener?)
    fun deleteOffer(offerId : String, listener : IOnGetDataListener?)
    fun getMyOffers(listener : IOnGetDataListener?)
    fun getContactOffers(listener: IOnGetDataListener?)
    fun getAcceptedOffers(listener: IOnGetDataListener?)
    fun getMyOffersToExecute(listener: IOnGetDataListener?)
    fun getOfferById(offerId: String, listener: IOnGetDataListener?)
    fun getFilteredOffers(category: String = "", costMin: Double = 0.0, costMax: Double = Double.MAX_VALUE,
                          geoPoint: GeoPoint? = null, radius: Double = 0.0, onComplete:(List<Offer>)->Unit)


    /**
     * Profile Functions
     */
    fun saveProfile(profile : Profile, listener: IOnGetDataListener?)
    fun registerProfile(profile: Profile, password : String, listener: IOnGetDataListener?)
    fun getProfile(uid : String, listener : IOnGetDataListener?)
    fun rateProfile(uid: String, rating: Double, listener: IOnGetDataListener?)
    /**
     * Get User ID
     */
    fun getCurrentId() : String

    /**
     * Chat Channels Functions
     */
    fun getOrCreateChatChannel(otherUserId: String, onComplete:(channelId:String)->Unit)
    fun addChatMessagesListener(channelId:String, context: Context,
                                onListen: (List<Item>)->Unit): ListenerRegistration
    fun fetchUserChats(onComplete:(profile:Profile)->Unit)
    fun sendMessage(message: TextMessage, channelId: String)
    fun addChatChannelsListener(context: Context, onListen: () -> Unit): ListenerRegistration
    fun deleteChatChannel(otherUserId: String, listener: IOnGetDataListener?)


    /**
     * Storage Functions
     */
    fun getImage(photoPath:String, onComplete:(bitmap:Bitmap)->Unit)
    fun uploadProfilePicture(bitmap: Bitmap, onSuccess:(imagePath:String)->Unit)
    fun uploadOfferPicture(bitmap: Bitmap, onSuccess:(imagePath:String)->Unit)
    fun pathToReference(path: String) : StorageReference

    /**
     * Categories Functions
     */

    fun getCategories(onComplete: (List<String> ) -> Unit)
}