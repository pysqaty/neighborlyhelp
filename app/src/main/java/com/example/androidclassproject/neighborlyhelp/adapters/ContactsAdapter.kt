package com.example.androidclassproject.neighborlyhelp.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.androidclassproject.neighborlyhelp.R
import com.example.androidclassproject.neighborlyhelp.holders.ContactViewHolder
import com.example.androidclassproject.neighborlyhelp.utils.IContracter

class ContactsAdapter(val ihide : IContracter, val myID: String) : RecyclerView.Adapter<ContactViewHolder>() {
    val data: MutableList<Pair<String, String>>

    init {
        data = ArrayList()
    }

    fun swapData(newData: List<Pair<String, String>>) {
        data.clear()
        data.addAll(newData)
        notifyDataSetChanged()
    }

    fun removeAt(position: Int) {
        data.removeAt(position)
        notifyItemRemoved(position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactViewHolder {
        val context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.contact_list_item, parent, false)
        return ContactViewHolder(view)
    }

    override fun onBindViewHolder(holder: ContactViewHolder, position: Int) = holder.bind(data[position], ihide, myID)
    override fun getItemCount() = data.size
}