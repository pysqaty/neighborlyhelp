package com.example.androidclassproject.neighborlyhelp.fragments

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.example.androidclassproject.neighborlyhelp.R
import com.google.firebase.auth.FirebaseAuth

class ChatFragment : Fragment(){


    fun replaceFragmnet(fragmentTag: String) {
        when(fragmentTag){
            getString(R.string.fragment_register_user) -> {
                val registerUserFragment = RegisterUserFragment()
                doFragmentTransaction(registerUserFragment, fragmentTag)
            }

            getString(R.string.fragment_login_user) -> {
                val loginUserFragment = LoginFragment()
                doFragmentTransaction(loginUserFragment, fragmentTag)
            }
            getString(R.string.fragment_latest_messages) -> {
                val latestMessagesFragment = LatestMessagesFragment()
                doFragmentTransaction(latestMessagesFragment, fragmentTag)
            }
        }
    }

    //function to check if the user is logged in
    private fun verifyUserLoggedIn( ) : Boolean{

        val uid = FirebaseAuth.getInstance().uid ?: return false

        return true
    }


    private fun doFragmentTransaction(fragment : Fragment, fragmentTag : String){
        childFragmentManager.beginTransaction().apply {
            replace(R.id.chat_fragment_container, fragment)
            addToBackStack(fragmentTag)
            commit()
        }
    }

    private var listener: OnFragmentInteractionListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        //check if the user is logged in
        val loggedIn = verifyUserLoggedIn()

        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_chat, container, false)
        val fragment : Fragment
        if(!loggedIn){
            fragment = LoginOrRegisterFragment()
        }
        else {
            fragment = LatestMessagesFragment()
        }
        childFragmentManager.beginTransaction().apply {
            add(R.id.chat_fragment_container, fragment)
            commit()
        }
        return view

    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    companion object {
        @JvmStatic
        fun newInstance() =
            ChatFragment().apply {
                arguments = Bundle().apply {
                }
            }
    }
}
