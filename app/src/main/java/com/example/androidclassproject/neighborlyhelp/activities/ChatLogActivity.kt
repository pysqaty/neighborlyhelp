package com.example.androidclassproject.neighborlyhelp.activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.Button
import android.widget.EditText
import com.example.androidclassproject.neighborlyhelp.R
import com.example.androidclassproject.neighborlyhelp.fragments.LatestMessagesFragment
import com.example.androidclassproject.neighborlyhelp.models.MessageItem
import com.example.androidclassproject.neighborlyhelp.models.TextMessage
import com.example.androidclassproject.neighborlyhelp.utils.FirebaseCommunicator
import com.google.firebase.firestore.ListenerRegistration
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.Section
import com.xwray.groupie.ViewHolder
import com.xwray.groupie.kotlinandroidextensions.Item
import java.sql.Timestamp

class ChatLogActivity : AppCompatActivity(){

    private fun sendMessage(){}

    lateinit var recyclerview : RecyclerView
    lateinit var sendBtn : Button
    lateinit var sendTxt : EditText
    private val adapter = GroupAdapter<ViewHolder>()
    private val db = FirebaseCommunicator.getInstance()
    private var shouldInitRecyclerView = true
    private lateinit var messagesSection: Section
    private lateinit var messagesListenerRegistration: ListenerRegistration

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat_log)
        supportActionBar?.title = intent.getStringExtra(LatestMessagesFragment.USER_NAME)
        val otherUserId = intent.getStringExtra(LatestMessagesFragment.USER_ID)
        sendTxt = findViewById(R.id.edittext_chat_log)
        sendBtn = findViewById(R.id.send_btn_chat_log)
        db.getOrCreateChatChannel(otherUserId){channelId ->
            messagesListenerRegistration =
                    db.addChatMessagesListener(channelId, this, this::updateRecyclerView)

            sendBtn.setOnClickListener{
                val time = Timestamp(System.currentTimeMillis())
                val message = TextMessage(sendTxt.text.toString(), time, db.getCurrentId())
                adapter.add(MessageItem(message, this))
                sendTxt.setText("")
                db.sendMessage(message, channelId)
            }


        }
        recyclerview = findViewById(R.id.recyclerview_chat_log)
        recyclerview.adapter = adapter
        recyclerview.scrollToPosition(recyclerview.adapter!!.itemCount - 1)
    }

    private fun updateRecyclerView(messages: List<Item>) {
        fun init() {
            recyclerview.apply {
                layoutManager = LinearLayoutManager(this@ChatLogActivity)
                adapter = GroupAdapter<ViewHolder>().apply {
                    messagesSection = Section(messages)
                    this.add(messagesSection)
                }
            }
            shouldInitRecyclerView = false
        }

        fun updateItems() = messagesSection.update(messages)

        if (shouldInitRecyclerView)
            init()
        else
            updateItems()

        recyclerview.scrollToPosition(recyclerview.adapter!!.itemCount - 1)
    }
}


