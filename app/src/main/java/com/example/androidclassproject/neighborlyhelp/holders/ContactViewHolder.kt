package com.example.androidclassproject.neighborlyhelp.holders

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.Button
import android.widget.TextView
import com.example.androidclassproject.neighborlyhelp.R
import com.example.androidclassproject.neighborlyhelp.utils.FirebaseCommunicator
import com.example.androidclassproject.neighborlyhelp.utils.IContracter

class ContactViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
    private var firebaseCommunicator = FirebaseCommunicator.getInstance()
    private var contactNick = view.findViewById<TextView>(R.id.contact_nick)
    private var acceptButton = view.findViewById<Button>(R.id.accept_contact_button)
    private var contactButton = view.findViewById<Button>(R.id.contact_button)


    fun bind(contact : Pair<String, String>, ihide : IContracter, myID: String) {
        contactNick.text = contact.first //nick --> second == id
        contactNick.setOnClickListener { ihide.showProfile(contact.second, myID) }
        acceptButton.setOnClickListener {
            ihide.accept(contact.second)
        }
        contactButton.setOnClickListener {
            ihide.contact(contact.second)
            contactButton.isEnabled = false
        }
    }
}