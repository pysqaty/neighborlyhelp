package com.example.androidclassproject.neighborlyhelp.fragments

interface IFragmentManagerActivity {
    fun replaceFragment(fragmentTag : String)
}