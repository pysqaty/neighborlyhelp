package com.example.androidclassproject.neighborlyhelp.adapters

import android.support.v4.app.FragmentManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.androidclassproject.neighborlyhelp.R
import com.example.androidclassproject.neighborlyhelp.enums.OfferCategory
import com.example.androidclassproject.neighborlyhelp.holders.OfferViewHolder
import com.example.androidclassproject.neighborlyhelp.models.Offer
import com.example.androidclassproject.neighborlyhelp.models.Profile
import com.example.androidclassproject.neighborlyhelp.utils.IRefreshable
import com.google.android.gms.maps.model.LatLng
import java.time.LocalDate
import java.util.*

class OffersAdapter(val fragmentView: View, val myId: String, val refreshable: IRefreshable): RecyclerView.Adapter<OfferViewHolder>() {
    val data: MutableList<Offer>

    init {
        data = ArrayList()
    }

    fun swapData(newData: List<Offer>) {
        data.clear()
        data.addAll(newData)
        notifyDataSetChanged()
    }

    fun removeAt(position: Int) {
        data.removeAt(position)
        notifyItemRemoved(position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OfferViewHolder {
        val context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.offer_list_item, parent, false)
        return OfferViewHolder(view, refreshable)
    }

    override fun onBindViewHolder(holder: OfferViewHolder, position: Int) = holder.bind(data[position], fragmentView, myId)
    override fun getItemCount() = data.size
}