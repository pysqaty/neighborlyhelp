package com.example.androidclassproject.neighborlyhelp.constants


const val GALLERY = 1
const val CAMERA = 2

const val FILTER_CATEGORY = "filter_cat"
const val FILTER_MIN_COST = "min_cost"
const val FILTER_MAX_COST = "max_cost"
const val FILTER_DISTANCE = "distance"