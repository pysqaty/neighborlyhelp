package com.example.androidclassproject.neighborlyhelp.fragments

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RatingBar
import android.widget.TextView
import android.widget.Toast
import com.example.androidclassproject.neighborlyhelp.utils.CircleImageView
import com.example.androidclassproject.neighborlyhelp.R
import com.example.androidclassproject.neighborlyhelp.activities.AddEditProfile
import com.example.androidclassproject.neighborlyhelp.models.EditProfileData
import com.example.androidclassproject.neighborlyhelp.models.Profile
import com.example.androidclassproject.neighborlyhelp.models.User
import com.example.androidclassproject.neighborlyhelp.utils.FirebaseCommunicator
import com.example.androidclassproject.neighborlyhelp.utils.IOnGetDataListener
import com.google.firebase.firestore.DocumentSnapshot

private const val EDIT = 3
private const val IS_YOUR_PROFILE_PARAM = "isYourProfileParam"
private const val PROFILE_ID_PARAM = "profileIDParam"
private const val SHOULD_DISPLAY_ALL_PROFILE = "shouldDisplayAllProfile"

class ProfileFragment : Fragment() {
    private var listener: OnFragmentInteractionListener? = null
    private var profileData: Profile? = null
    private var isYourProfile: Boolean? = null
    private var shouldDisplayAllProfile: Boolean? = null
    private var profileID: String? = null
    private var layoutView: View? = null
    private lateinit var fbCommunicator: FirebaseCommunicator


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            profileID = it.getString(PROFILE_ID_PARAM)
            isYourProfile = it.getBoolean(IS_YOUR_PROFILE_PARAM, false)
            shouldDisplayAllProfile = it.getBoolean(SHOULD_DISPLAY_ALL_PROFILE, true)
        }

        fbCommunicator = FirebaseCommunicator.getInstance()
    }

    @SuppressLint("RestrictedApi")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        layoutView = inflater.inflate(R.layout.fragment_profile, container, false)
        if(!isYourProfile!!)
            layoutView!!.findViewById<FloatingActionButton>(R.id.edit_profile_button).visibility = View.GONE

        val dbListener = object : IOnGetDataListener {
            override fun onSuccess(data: Any?) {
                val userData = (data as DocumentSnapshot).toObject(Profile::class.java)
                profileData = userData
                setBasicInformation(layoutView!!)
                //if more information needed
                checkAndSetMoreInformation()

                //if is your profile
                if(isYourProfile!!) {
                    val editProfile = layoutView!!.findViewById<FloatingActionButton>(R.id.edit_profile_button)

                    editProfile.setOnClickListener { startEditActivity() }
                    editProfile.visibility = View.VISIBLE
                }

            }

            override fun onFailure() {
                Toast.makeText(context,"Pobieranie profilu nie powiodło się",Toast.LENGTH_SHORT).show()
            }
        }
        fbCommunicator.getProfile(profileID!!, dbListener)
        return layoutView
    }

    private fun startEditActivity() {
        val editIntent = Intent(context, AddEditProfile::class.java)
        editIntent.putExtra(PROFILE_DATA_PARAM, profileData!!.getEditProfileData())
        startActivityForResult(editIntent, EDIT)
    }

    private fun setBasicInformation(layoutView : View) {
        val ratingBar = layoutView.findViewById<RatingBar>(R.id.rating_bar)
        val ratingText = layoutView.findViewById<TextView>(R.id.rating_text)
        ratingBar.rating = profileData!!.rating.toFloat()
        ratingText.text = ratingBar.rating.toString()

        layoutView.findViewById<TextView>(R.id.nick_name).text = profileData!!.nickname
        if(!profileData!!.photoPath!!.isEmpty()) {
            fbCommunicator.getImage(profileData!!.photoPath!!) { bmp : Bitmap ->  layoutView.findViewById<CircleImageView>(R.id.profile_photo).setImageBitmap(bmp)}
        }
    }


    private fun checkAndSetMoreInformation() {
        if (shouldDisplayAllProfile!!) {
            val moreProfileInformation = MoreProfileInformation.newInstance(profileData!!.getEditProfileData())
            fragmentManager!!.beginTransaction().add(R.id.more_profile_information, moreProfileInformation).commit()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == EDIT && resultCode == Activity.RESULT_OK) {
            val editProfileData = data!!.extras?.getParcelable<EditProfileData>(PROFILE_DATA_PARAM)
            profileData!!.setEditProfileData(editProfileData!!)

            val listener = object : IOnGetDataListener {
                override fun onSuccess(data: Any?) {
                    setBasicInformation(layoutView!!)
                    checkAndSetMoreInformation()
                    Toast.makeText(context,"Zapisano profil", Toast.LENGTH_SHORT).show()
                }

                override fun onFailure() {
                    Toast.makeText(context,"Wystąpił błąd przy zapisywaniu profilu", Toast.LENGTH_SHORT).show()
                }
            }
            fbCommunicator.saveProfile(profileData!!, listener)
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }


    companion object {
        const val PROFILE_DATA_PARAM = "profileDataParam"
        @JvmStatic
        fun newInstance(profileID: String, isYourProfile: Boolean, shouldDisplayAllProfile :Boolean) =
            ProfileFragment().apply {
                arguments = Bundle().apply {
                    putString(PROFILE_ID_PARAM, profileID)
                    putBoolean(IS_YOUR_PROFILE_PARAM, isYourProfile)
                    putBoolean(SHOULD_DISPLAY_ALL_PROFILE, shouldDisplayAllProfile)
                }
            }
    }
}
