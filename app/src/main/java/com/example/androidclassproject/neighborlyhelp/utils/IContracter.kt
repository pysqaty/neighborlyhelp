package com.example.androidclassproject.neighborlyhelp.utils

interface IContracter {
    fun contact(id: String)
    fun accept(id: String)
    fun showProfile(id: String, myID: String)
}