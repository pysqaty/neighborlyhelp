package com.example.androidclassproject.neighborlyhelp.fragments

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.example.androidclassproject.neighborlyhelp.R
import com.example.androidclassproject.neighborlyhelp.activities.ChatLogActivity
import com.example.androidclassproject.neighborlyhelp.models.ChatItem
import com.example.androidclassproject.neighborlyhelp.models.Profile
import com.example.androidclassproject.neighborlyhelp.models.User
import com.example.androidclassproject.neighborlyhelp.utils.FirebaseCommunicator
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.GeoPoint
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.chat_row_latest_message.view.*
import org.jetbrains.anko.support.v4.toast

class LatestMessagesFragment : Fragment() {
    private var listener: OnFragmentInteractionListener? = null
    lateinit var recyclerView : RecyclerView
    private val db = FirebaseCommunicator.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_latest_messages, container, false)

        recyclerView = view.findViewById(R.id.recyclerview_latestmessages)
        db.addChatChannelsListener(context!!,this::fetchChats)

        Log.i("RecentMessage", "Fetching Chats")
        fetchChats()

        return view
    }


    private fun fetchChats() {
        val adapter = GroupAdapter<ViewHolder>()
        adapter.setOnItemClickListener { item, view ->
            val chatItem = item as ChatItem
            val intent = Intent(view.context, ChatLogActivity::class.java)
            intent.putExtra(USER_NAME, chatItem.user.nickname)
            intent.putExtra(USER_ID, chatItem.user.id)
            startActivity(intent)
        }
        recyclerView.adapter = adapter
        db.fetchUserChats { profile ->
            adapter.add(ChatItem(profile, this.context!!))
        }

    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }


    companion object {
        @JvmStatic
        val USER_NAME = "USER_NAME"
        val USER_ID = "USER_ID"
        fun newInstance() =
            LatestMessagesFragment().apply {
                arguments = Bundle().apply {

                }
            }
    }
}

