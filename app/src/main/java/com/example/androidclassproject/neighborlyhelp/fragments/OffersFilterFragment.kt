package com.example.androidclassproject.neighborlyhelp.fragments

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.Editable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.SeekBar
import android.widget.SeekBar.OnSeekBarChangeListener
import com.example.androidclassproject.neighborlyhelp.R
import com.example.androidclassproject.neighborlyhelp.constants.FILTER_CATEGORY
import com.example.androidclassproject.neighborlyhelp.constants.FILTER_DISTANCE
import com.example.androidclassproject.neighborlyhelp.constants.FILTER_MAX_COST
import com.example.androidclassproject.neighborlyhelp.constants.FILTER_MIN_COST
import com.example.androidclassproject.neighborlyhelp.utils.FirebaseCommunicator
import kotlinx.android.synthetic.main.fragment_offers_filter.*
import kotlinx.android.synthetic.main.fragment_offers_filter.view.*


private const val PROFILE_ID_PARAM = "PROFILE_ID"
class OffersFilterFragment : Fragment(), AdapterView.OnItemSelectedListener {

    override fun onNothingSelected(parent: AdapterView<*>?) {
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
    }

    private var listener: OnFragmentInteractionListener? = null
    private var myId: String? = null
    private val firebaseCommunicator: FirebaseCommunicator = FirebaseCommunicator.getInstance()
    val tempCategories : MutableList<String> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            myId = it.getString(PROFILE_ID_PARAM)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.fragment_offers_filter, container, false)

        val sharedPref = activity!!.getPreferences(Context.MODE_PRIVATE)
        val minCost = sharedPref.getFloat(FILTER_MIN_COST, -1.0f)
        rootView.offer_mincost.text = if(minCost == -1.0f) {
            Editable.Factory.getInstance().newEditable("")
        } else {
            Editable.Factory.getInstance().newEditable(minCost.toString())
        }
        val maxCost = sharedPref.getFloat(FILTER_MAX_COST, Float.MAX_VALUE)
        rootView.offer_maxcost.text = if(maxCost == Float.MAX_VALUE) {
            Editable.Factory.getInstance().newEditable("")
        } else {
            Editable.Factory.getInstance().newEditable(maxCost.toString())
        }
        firebaseCommunicator.getCategories { categories ->
            category.onItemSelectedListener = this

            tempCategories.add("")
            tempCategories.addAll(categories)
            ArrayAdapter(activity!!.applicationContext, android.R.layout.simple_spinner_item, tempCategories)
                .also { adapter ->
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                    category.adapter = adapter
                }
            val index = tempCategories.indexOfFirst { value -> value ==  sharedPref.getString(FILTER_CATEGORY, "")}
            rootView.category.setSelection(index, true)
            rootView.confirm_button.setOnClickListener {
                val sharedPref = activity!!.getPreferences(Context.MODE_PRIVATE)
                val editor = sharedPref.edit()
                editor.putString(FILTER_CATEGORY, category.selectedItem.toString())
                val minCost = offer_mincost.text.toString().toFloatOrNull()
                val maxCost = offer_maxcost.text.toString().toFloatOrNull()
                editor.putFloat(FILTER_MIN_COST, minCost ?: -1.0f)
                editor.putFloat(FILTER_MAX_COST, maxCost ?: Float.MAX_VALUE)

                if(rootView.filter_distance.progress == rootView.filter_distance.max) {
                    editor.putFloat(FILTER_DISTANCE, Float.MAX_VALUE)
                }
                else {
                    editor.putFloat(FILTER_DISTANCE, rootView.filter_distance.progress.toFloat())
                }

                editor.commit()
                parentFragment!!.childFragmentManager.beginTransaction().replace(R.id.offers_fragments_container, OffersListsFragment.newInstance(myId!!)).commit()
            }
        }
        val lastFilterDist = sharedPref.getFloat(FILTER_DISTANCE, Float.MAX_VALUE)
        if(lastFilterDist == Float.MAX_VALUE) {
            rootView.filter_distance.progress = rootView.filter_distance.max
            rootView.current_value_distance_filter.text = rootView.filter_distance.progress.toString() + "+"

        } else {
            rootView.filter_distance.progress = lastFilterDist.toInt()
            rootView.current_value_distance_filter.text = rootView.filter_distance.progress.toString()

        }

        rootView.filter_distance.setOnSeekBarChangeListener(object : OnSeekBarChangeListener {
            var progress = 0

            override fun onProgressChanged(seekBar: SeekBar, progresValue: Int, fromUser: Boolean) {
                progress = progresValue
                if(progress == seekBar.max) {
                    rootView.current_value_distance_filter.text = progress.toString() + "+"
                }
                else {
                    rootView.current_value_distance_filter.text = progress.toString()
                }
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {
            }

            override fun onStopTrackingTouch(seekBar: SeekBar) {
            }
        })
        return rootView
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }


    companion object {
        @JvmStatic
        fun newInstance(myId: String) =
            OffersFilterFragment().apply {
                arguments = Bundle().apply {
                    putString(PROFILE_ID_PARAM, myId)
                }
            }
    }
}
