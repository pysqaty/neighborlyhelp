package com.example.androidclassproject.neighborlyhelp.activities

import android.Manifest
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import com.example.androidclassproject.neighborlyhelp.R
import com.example.androidclassproject.neighborlyhelp.fragments.*
import com.google.firebase.auth.FirebaseAuth
import android.widget.Toast
import android.content.pm.PackageManager



class LoginOrRegisterActivity : AppCompatActivity(), OnFragmentInteractionListener, IFragmentManagerActivity {
    override fun replaceFragment(fragmentTag: String) {
        when(fragmentTag){
            getString(R.string.fragment_register_user) -> {
                val registerUserFragment = RegisterUserFragment()
                doFragmentTransaction(registerUserFragment, fragmentTag)
            }

            getString(R.string.fragment_login_user) -> {
                val loginUserFragment = LoginFragment()
                doFragmentTransaction(loginUserFragment, fragmentTag)
            }
        }
    }

    //function to check if the user is logged in
    private fun verifyUserLoggedIn( ) : Boolean{
        FirebaseAuth.getInstance().uid ?: return false
        return true
    }

    private fun doFragmentTransaction(fragment : Fragment, fragmentTag : String){
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.log_reg_container, fragment)
            addToBackStack(fragmentTag)
            commit()
        }
    }

    override fun onFragmentInteraction() {}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login_or_register)

        if(verifyUserLoggedIn()){
            var intent = Intent(this, MainActivity::class.java)
            finish()
            startActivity(intent)
        }


        val transaction = supportFragmentManager.beginTransaction()
        val fragment = LoginOrRegisterFragment()
        transaction.add(R.id.log_reg_container, fragment)
        transaction.commit()

        ActivityCompat.requestPermissions(this,
             arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE,
                 Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.INTERNET),
            1)

    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>, grantResults: IntArray
    ) {
        when (requestCode) {
            1 -> {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.isNotEmpty() && grantResults.any { value -> value != PackageManager.PERMISSION_GRANTED }) {

                    Toast.makeText(
                        this,
                        "Aplikacja może nie działać poprawnie bez wszystkich uprawnień",
                        Toast.LENGTH_LONG
                    ).show()
                } else {
                    //all permissions granted
                }
                return
            }
        }// other 'case' lines to check for other
        // permissions this app might request
    }
}
