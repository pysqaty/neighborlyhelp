package com.example.androidclassproject.neighborlyhelp.utils

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Log
import com.example.androidclassproject.neighborlyhelp.models.*
import com.example.androidclassproject.neighborlyhelp.models.Offer
import com.example.androidclassproject.neighborlyhelp.models.Profile
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.*
import com.google.firebase.storage.FirebaseStorage
import com.xwray.groupie.kotlinandroidextensions.Item
import java.io.ByteArrayOutputStream
import java.util.*
import com.google.firebase.firestore.FirebaseFirestore
import org.imperiumlabs.geofirestore.GeoFirestore
import org.imperiumlabs.geofirestore.GeoQuery
import org.imperiumlabs.geofirestore.GeoQueryEventListener
import java.lang.Exception

/**
 * Singleton Class implementing [IDBCommunicator] interface
 * used to communicate with Firebase DB
 * Example initialization:
 *
 *  val db : IDBCommunicator = FirebaseCommunicator(context!!)
 */


class FirebaseCommunicator private constructor() : IDBCommunicator {


    override fun getOfferById(offerId: String, listener: IOnGetDataListener?) {
        offersCollection.document(offerId).get()
            .addOnSuccessListener {result ->
                listener?.onSuccess(result)
            }
            .addOnFailureListener{
                listener?.onFailure()
            }
    }

    /**
     * Function to register new profile. Should be called to create a new account.
     * The use of User class is only temporary
     */
    override fun registerProfile(profile: Profile, password : String, listener: IOnGetDataListener?) {
        auth.createUserWithEmailAndPassword(profile.email!!, password)
            .addOnSuccessListener {result ->
                profile.id = auth.uid!!
                saveProfile(profile,null)
                listener?.onSuccess(result)
                currentUserStorageRef = storage.reference.child(auth.uid!!)
                currentUserDocRef = db.collection("users").document(auth.uid!!)
            }
            .addOnFailureListener{
                listener?.onFailure()
            }
    }

    override fun rateProfile(uid: String, rating: Double, listener: IOnGetDataListener?) {
        usersCollection.document(uid).get().addOnSuccessListener {
            usersCollection.document(uid)
                .update("rating", rating, "ratesCounter", (it["ratesCounter"] as Long) + 1)
                .addOnSuccessListener {
                    listener?.onSuccess(it)
                }
                .addOnFailureListener {
                    listener?.onFailure()
                }

        }

    }


    /**
     * Function to save profile. Can be used to update existing profile.
     * The use of User class is only temporary
     */
    override fun saveProfile(profile: Profile, listener: IOnGetDataListener?) {
        usersCollection.document(profile.id).set(profile)
            .addOnSuccessListener {result ->
                val id = profile.id
                listener?.onSuccess(result)
            }
            .addOnFailureListener{
                listener?.onFailure()
            }
    }

    override fun uploadProfilePicture(bitmap: Bitmap, onSuccess:(imagePath:String)->Unit){
        val baos = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
        val data = baos.toByteArray()
        val ref = currentUserStorageRef?.child("profilePictures/${UUID.nameUUIDFromBytes(data)}")
        ref?.putBytes(data)
            ?.addOnSuccessListener {
                onSuccess(ref.path)
            }
    }

    override fun uploadOfferPicture(bitmap: Bitmap, onSuccess:(imagePath:String)->Unit){
        val baos = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
        val data = baos.toByteArray()
        val ref = offersImagesRef.child("${UUID.nameUUIDFromBytes(data)}")
        ref.putBytes(data)
            .addOnSuccessListener {
                onSuccess(ref.path)
            }
    }

    override fun pathToReference(path: String) = storage.getReference(path)


    override fun getCategories(onComplete: (List<String>) -> Unit) {
        categoriesDocument.get()
            .addOnSuccessListener {
                val list = it["offer_categories"] as List<String>
                onComplete(list)
            }
    }

    /**
     * Function to retrieve a profile from the DB byt its id.
     * The listener must implement [IOnGetDataListener] interface.
     * [onSuccess] function of the listener is used upon successful data retrieval
     * so data manipulation code should be placed there.
     * Example listener:
     *
     *  val list : ArrayList<String> = ArrayList()
     *  val listener = object : IOnGetDataListener {
        override fun onSuccess(data: Any) {
        val profile = (data as DocumentSnapshot).toObject(User::class.java)
        list.add(profile!!.nickname)
        Log.d("PROFILE", list[0])}}
     *
     * Listener can interact with objects outside of its scope.
     * Example call:
     *
     *   db.getProfile(uid,listener)
     */
    override fun getProfile(uid: String, listener : IOnGetDataListener?){
        usersCollection.document(uid).get()
            .addOnSuccessListener {result ->
                listener?.onSuccess(result)
            }
            .addOnFailureListener{
                listener?.onFailure()
            }
    }

    /**
     * Function to get currently logged user uid.
     */
    override fun getCurrentId(): String {return auth.uid!!}


    /**
     * Function to save an offer in the DB.
     */
    override fun saveOffer(offer : Offer, listener: IOnGetDataListener?) {
        if(offer.id == "") {
            val ref = offersCollection.document()
            offer.id = ref.id
        }
        db.collection("offers").document(offer.id).set(offer)
            .addOnSuccessListener {result ->
                geo.setLocation(offer.id, GeoPoint(offer.location.latitude, offer.location.longitude))
                listener?.onSuccess(result)
            }
            .addOnFailureListener{
                listener?.onFailure()
            }
    }

    /**
     * Function to get all offers created by user with [uid].
     * Takes listener parameter just like [getProfile] function.
     */
    override fun getUserOffers(uid : String, listener : IOnGetDataListener?) {
        offersCollection.whereEqualTo("issuer", uid).get()
            .addOnSuccessListener {result ->
                listener?.onSuccess(result)
            }
            .addOnFailureListener {
                listener?.onFailure()
            }
    }

    override fun deleteOffer(offerId : String, listener : IOnGetDataListener?){
        offersCollection.document(offerId).delete()
            .addOnSuccessListener {
                listener?.onSuccess(it)
            }
        geo.removeLocation(offerId)
    }

    /**
     * Function to get all offers from the DB.
     * Created for test purposes.
     * Takes listener parameter just like [getProfile] function.
     */
    override fun getAllOffers(listener: IOnGetDataListener?) {
        val id = getCurrentId()
        offersCollection.whereGreaterThan("issuer", id).whereEqualTo("executor", "").get()
            .addOnSuccessListener {result ->
                listener?.onSuccess(result)
            }
            .addOnFailureListener {
                listener?.onFailure()
            }
        offersCollection.whereLessThan("issuer", id).whereEqualTo("executor", "").get()
            .addOnSuccessListener {result ->
                listener?.onSuccess(result)
            }
            .addOnFailureListener {
                listener?.onFailure()
            }
    }

    private fun getMySpecialOffers(role:String, listener: IOnGetDataListener?){
        offersCollection.whereEqualTo(role, getCurrentId()).get()
            .addOnSuccessListener {
                listener?.onSuccess(it)
            }
            .addOnFailureListener{
                listener?.onFailure()
            }
    }

    override fun getMyOffers(listener: IOnGetDataListener?) {
        getMySpecialOffers("issuer", listener)
    }

    override fun getMyOffersToExecute(listener: IOnGetDataListener?) {
        getMySpecialOffers("executor", listener)
    }

    private fun getSpecialOffers(list:String, listener: IOnGetDataListener?){
        offersCollection.whereArrayContains(list, getCurrentId()).get()
            .addOnSuccessListener {
                listener?.onSuccess(it)
            }
            .addOnFailureListener{
                listener?.onFailure()
            }
    }

    override fun getContactOffers(listener: IOnGetDataListener?) {
        getSpecialOffers("contacts", listener)
    }


    override fun getAcceptedOffers(listener: IOnGetDataListener?) {
        getSpecialOffers("accepted", listener)
    }


    private fun getOffersCatCost(category: String = "",
                                 costMin: Double = 0.0, costMax:Double = 0.0, onComplete:(List<Offer>)->Unit){
        val list : MutableList<Offer> = mutableListOf()
        if(category != "")
            offersCollection
                .whereEqualTo("category", category)
                .whereGreaterThan("cost", costMin)
                .whereLessThan("cost", costMax)
                .get()
                .addOnSuccessListener {
                    for(d in it.documents){
                        list.add(d.toObject(Offer::class.java)!!)
                    }
                    onComplete(list)
                }
        else{
            offersCollection
                .whereGreaterThan("cost", costMin)
                .whereLessThan("cost", costMax)
                .get()
                .addOnSuccessListener {
                    for(d in it.documents){
                        list.add(d.toObject(Offer::class.java)!!)
                    }
                    onComplete(list)
                }
        }
    }


    override fun getFilteredOffers(category: String, costMin: Double, costMax:Double,
                                   geoPoint: GeoPoint?, radius: Double, onComplete:(List<Offer>)->Unit) {
        if(geoPoint == null) {
            getOffersCatCost(category, costMin, costMax, onComplete)
        }
        else {
            val geoQuery = geo.queryAtLocation(geoPoint, radius)
            geoQuery.addGeoQueryEventListener(object :GeoQueryEventListener{

                override fun onGeoQueryReady() {
                    geoQuery.removeAllListeners()
                }

                override fun onKeyEntered(p0: String?, p1: GeoPoint?) {
                    if (p0 != null) {
                        val list : MutableList<Offer> = mutableListOf()
                        offersCollection.document(p0).get()
                            .addOnSuccessListener {
                                val offer = it.toObject(Offer::class.java)
                                if(category != "" && offer!!.category != category)
                                    return@addOnSuccessListener
                                if(offer!!.cost in costMin..costMax) {
                                    list.add(it.toObject(Offer::class.java)!!)
                                    onComplete(list)
                                    geoQuery.removeAllListeners()
                                }

                            }
                    }
                }
                override fun onKeyMoved(p0: String?, p1: GeoPoint?) {}

                override fun onKeyExited(p0: String?) {}

                override fun onGeoQueryError(p0: Exception?) {}

            })
        }
    }




    override fun getOrCreateChatChannel(otherUserId: String, onComplete:(channelId:String)->Unit) {
        db.collection("users").document(auth.uid!!).collection("engagedChatChannels")
            .document(otherUserId)
            .get().addOnSuccessListener {
                                if(it.exists()) {
                    onComplete(it["channelId"] as String)
                    return@addOnSuccessListener
                }
                val newChannel = chatChannelsCollection.document()
                newChannel.set(ChatChannel(mutableListOf(getCurrentId(),otherUserId)))

                currentUserDocRef!!
                    .collection("engagedChatChannels")
                    .document(otherUserId)
                    .set(mapOf("channelId" to newChannel.id))

                usersCollection.document(otherUserId)
                    .collection("engagedChatChannels")
                    .document(getCurrentId())
                    .set(mapOf("channelId" to newChannel.id))

                onComplete(newChannel.id)
            }

    }

    override fun deleteChatChannel(otherUserId: String, listener: IOnGetDataListener?) {
        usersCollection
            .whereEqualTo("id", otherUserId)
            .get()
            .addOnSuccessListener {
                for(d in it.documents){
                    d.reference.collection("engagedChatChannels").document(getCurrentId()).delete()
                }
            }
        val ref = currentUserDocRef!!.collection("engagedChatChannels").document(otherUserId)
        ref.get()
            .addOnSuccessListener {
                val chatId = it["channelId"]
                if(chatId is String) {
                    chatChannelsCollection.document(chatId as String).delete()
                    ref.delete()
                        .addOnSuccessListener { listener?.onSuccess(null) }
                        .addOnFailureListener { listener?.onFailure() }
                }
            }
    }

    override fun addChatMessagesListener(channelId:String, context: Context,
                                onListen: (List<Item>)->Unit):ListenerRegistration{
        return chatChannelsCollection.document(channelId).collection("messages")
            .orderBy("time")
            .addSnapshotListener {querySnapshot, firebaseFirestoreException ->
                if(firebaseFirestoreException != null){
                    Log.e("FIRESTORE", "ChateMessageListener error.", firebaseFirestoreException)
                    return@addSnapshotListener
                }

                val items = mutableListOf<Item>()
                querySnapshot?.documents?.forEach{
                    items.add(MessageItem(it.toObject(TextMessage::class.java)!!, context))
                }
                onListen(items)
            }
    }

    override fun addChatChannelsListener(context: Context, onListen: () -> Unit): ListenerRegistration {
        return currentUserDocRef!!.collection("engagedChatChannels")
            .addSnapshotListener { querySnapshot, firebaseFirestoreException ->
                if (firebaseFirestoreException != null) {
                    Log.e("FIRESTORE", "Users listener error.", firebaseFirestoreException)
                    return@addSnapshotListener
                }
                onListen()
            }
    }

    override fun fetchUserChats(onComplete:(profile:Profile)->Unit){
        currentUserDocRef!!
            .collection("engagedChatChannels")
            .get()
            .addOnSuccessListener {
                for(d in it.documents)
                    usersCollection.document(d.id).get()
                        .addOnSuccessListener {
                        onComplete(it.toObject(Profile::class.java)!!)
                    }
            }

    }

    override fun getImage(photoPath: String, onComplete:(bitmap:Bitmap)->Unit) {
        val ref = storageRef.child(photoPath)
        val ONE_MEGABYTE: Long = 1024 * 1024
        ref.getBytes(ONE_MEGABYTE)
            .addOnSuccessListener {
                val bitmap = BitmapFactory.decodeByteArray(it,0,it.size)
                onComplete(bitmap)
            }
    }

    override fun sendMessage(message: TextMessage, channelId: String){
        chatChannelsCollection.document(channelId)
            .collection("messages")
            .add(message)
    }

    companion object{
        fun getInstance():FirebaseCommunicator{
            if(instance == null){
                instance = FirebaseCommunicator()
            }
            return instance!!
        }
        private var instance : FirebaseCommunicator? = null
        @SuppressLint("StaticFieldLeak")
        private val db = FirebaseFirestore.getInstance()
        private val auth = FirebaseAuth.getInstance()
        private val storage = FirebaseStorage.getInstance()
        private val storageRef = storage.reference
        private var currentUserStorageRef = if(auth.uid != null) storage.reference.child(auth.uid!!) else null
        private val offersImagesRef = storageRef.child("offers")
        private val usersCollection = db.collection("users")
        private val offersCollection = db.collection("offers")
        private val chatChannelsCollection = db.collection("chats")
        private val categoriesDocument = db.collection("categories").document("list")
        private var currentUserDocRef = if(auth.uid != null) db.collection("users").document(auth.uid!!) else null
        private val geo = GeoFirestore(offersCollection)
    }
}


