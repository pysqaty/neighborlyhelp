package com.example.androidclassproject.neighborlyhelp.fragments


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView

import com.example.androidclassproject.neighborlyhelp.R
import com.example.androidclassproject.neighborlyhelp.models.EditProfileData
import com.example.androidclassproject.neighborlyhelp.models.Profile

private const val MORE_PROFILE_DATA_PARAM = "profileDataParam"
class MoreProfileInformation : Fragment() {
    private var profileData: EditProfileData? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            profileData = it.getParcelable(MORE_PROFILE_DATA_PARAM)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val layoutView = inflater.inflate(R.layout.fragment_more_profile_information, container, false)
        layoutView.findViewById<TextView>(R.id.name).text = profileData!!.firstName
        layoutView.findViewById<TextView>(R.id.surname).text = profileData!!.lastName
        layoutView.findViewById<TextView>(R.id.mail).text = profileData!!.email
        layoutView.findViewById<TextView>(R.id.phone).text = profileData!!.phoneNumber
        return layoutView
    }


    companion object {
        @JvmStatic
        fun newInstance(profileData : EditProfileData) =
            MoreProfileInformation().apply {
                arguments = Bundle().apply {
                    putParcelable(MORE_PROFILE_DATA_PARAM, profileData)
                }
            }
    }
}
