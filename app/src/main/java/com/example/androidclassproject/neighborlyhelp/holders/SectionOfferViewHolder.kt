package com.example.androidclassproject.neighborlyhelp.holders

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView

class SectionOfferViewHolder(view: View, mTextResourceid: Int) : RecyclerView.ViewHolder(view) {

    var title: TextView

    init {
        title = view.findViewById(mTextResourceid)
    }
}