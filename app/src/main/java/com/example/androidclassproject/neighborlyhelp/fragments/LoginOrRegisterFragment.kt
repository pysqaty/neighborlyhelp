package com.example.androidclassproject.neighborlyhelp.fragments

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.example.androidclassproject.neighborlyhelp.R

class LoginOrRegisterFragment : Fragment(), View.OnClickListener {
    override fun onClick(v: View?) {
        val list : ArrayList<String> = ArrayList()
        when(v?.id){
            R.id.register_user_btn ->
                mIFragmentManagerActivity.replaceFragment(getString(R.string.fragment_register_user))

            R.id.login_user_btn ->
                mIFragmentManagerActivity.replaceFragment(getString(R.string.fragment_login_user))
        }
    }
    //widgets
    lateinit var registerUserBtn : Button
    lateinit var loginUserBtn : Button

    //vars
    private var listener: OnFragmentInteractionListener? = null
    private lateinit var mIFragmentManagerActivity: IFragmentManagerActivity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_login_or_register, container, false)
        registerUserBtn = view.findViewById(R.id.register_user_btn)
        registerUserBtn.setOnClickListener(this)

        loginUserBtn = view.findViewById(R.id.login_user_btn)
        loginUserBtn.setOnClickListener(this)
        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mIFragmentManagerActivity = activity as IFragmentManagerActivity
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }
    companion object {
        @JvmStatic
        fun newInstance() =
            LoginOrRegisterFragment().apply {
                arguments = Bundle().apply {
                }
            }
    }
}
