package com.example.androidclassproject.neighborlyhelp.fragments

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.location.Location
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import com.example.androidclassproject.neighborlyhelp.R
import com.example.androidclassproject.neighborlyhelp.activities.AddEditOffer
import com.example.androidclassproject.neighborlyhelp.adapters.OffersAdapter
import com.example.androidclassproject.neighborlyhelp.adapters.SectionedOffersAdapter
import com.example.androidclassproject.neighborlyhelp.constants.FILTER_CATEGORY
import com.example.androidclassproject.neighborlyhelp.constants.FILTER_DISTANCE
import com.example.androidclassproject.neighborlyhelp.constants.FILTER_MAX_COST
import com.example.androidclassproject.neighborlyhelp.constants.FILTER_MIN_COST
import com.example.androidclassproject.neighborlyhelp.models.Offer
import com.example.androidclassproject.neighborlyhelp.models.Section
import com.example.androidclassproject.neighborlyhelp.utils.FirebaseCommunicator
import com.example.androidclassproject.neighborlyhelp.utils.IOnGetDataListener
import com.example.androidclassproject.neighborlyhelp.utils.IRefreshable
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.firebase.firestore.GeoPoint
import com.google.firebase.firestore.QuerySnapshot
import kotlinx.android.synthetic.main.fragment_offers.view.*


private const val ADD_OFFER = 3
private const val PROFILE_ID_PARAM = "profileIDParam"

class OffersListsFragment : Fragment(), AdapterView.OnItemSelectedListener, IRefreshable {
    private lateinit var layoutManager: RecyclerView.LayoutManager
    private lateinit var adapter: OffersAdapter
    private lateinit var sectionedAdapter: SectionedOffersAdapter
    lateinit var firebaseCommunicator: FirebaseCommunicator
    private var listener: OnFragmentInteractionListener? = null
    private lateinit var fusedLocationClient: FusedLocationProviderClient

    private var allOffers: List<Offer> = ArrayList()
    private var myOffers: List<Offer> = ArrayList()
    private var todoOffers: List<Offer> = ArrayList()
    private var currentList: Int = 0
    private var myId: String? = null
    private lateinit var rootView: View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            myId = it.getString(PROFILE_ID_PARAM)
        }
        firebaseCommunicator = FirebaseCommunicator.getInstance()
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(activity as Activity)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        rootView = inflater.inflate(R.layout.fragment_offers, container, false)
        val spinner: Spinner = rootView.findViewById(R.id.list_picker)
        spinner.onItemSelectedListener = this
        ArrayAdapter.createFromResource(
            activity!!.applicationContext,
            R.array.lists_types_array,
            R.layout.spinner_lists_item
        ).also { adapter ->
            adapter.setDropDownViewResource(R.layout.dropdown_spinner_lists_item)
            spinner.adapter = adapter
        }

        layoutManager = LinearLayoutManager(rootView.context)
        rootView.offers_recycler_view.layoutManager = layoutManager
        rootView.offers_recycler_view.setHasFixedSize(true)
        rootView.offers_recycler_view.setItemViewCacheSize(20)
        rootView.offers_recycler_view.isDrawingCacheEnabled = true
        rootView.offers_recycler_view.drawingCacheQuality = View.DRAWING_CACHE_QUALITY_HIGH
        adapter = OffersAdapter(rootView, myId!!, this)


        sectionedAdapter = SectionedOffersAdapter(activity as Context, R.layout.section, R.id.section_text, adapter)
        rootView.offers_recycler_view.adapter = sectionedAdapter

        rootView.add_offer_button.setOnClickListener {
            startAddEditOfferActivity()
        }

        rootView.filter_button.setOnClickListener {
            (parentFragment as OffersFragment).openFilterFragment()
        }

        rootView.swipe_refresh_layout.setOnRefreshListener {
            populateRightList()
        }

        return rootView
    }

    override fun Refresh() {
        populateRightList()
    }


    @SuppressLint("MissingPermission")
    private fun populateRightList() {
        when(currentList) {
            0 -> {
                val sections = emptyArray<Section>()
                val tmpList : MutableList<Offer> = mutableListOf()
                val sharedPref = activity!!.getPreferences(Context.MODE_PRIVATE)
                var latitude : Double = 0.0
                var longitude : Double = 0.0

                fusedLocationClient.lastLocation
                    .addOnSuccessListener { location: Location? ->
                        latitude =  if(location?.latitude == null) 0.0 else location.latitude
                        longitude = if(location?.longitude == null) 0.0 else location.longitude
                        val test = sharedPref.getFloat(FILTER_DISTANCE, Float.MAX_VALUE).toDouble()
                        firebaseCommunicator.getFilteredOffers(
                            sharedPref.getString(FILTER_CATEGORY, "")!!,
                            sharedPref.getFloat(FILTER_MIN_COST, -1.0f).toDouble(),
                            sharedPref.getFloat(FILTER_MAX_COST, Float.MAX_VALUE).toDouble(),
                            GeoPoint(latitude, longitude),
                            //null,
                            test
                        ) {
                            run {
                                val rightAllOffers = getAllOffers(it)
                                tmpList.addAll(rightAllOffers)
                                sectionedAdapter.setSections(sections)
                                adapter.swapData(tmpList)
                                rootView.swipe_refresh_layout.isRefreshing = false
                                sectionedAdapter.notifyDataSetChanged()
                            }
                        }
                    }
            }
            1 -> {
                val dbListener = object : IOnGetDataListener {
                    override fun onSuccess(data: Any?) {
                        myOffers = (data as QuerySnapshot).documents.map { v -> v.toObject(Offer::class.java)!! }
                        val tmpOffersTriple = getMyOffers(myOffers)
                        val sections = arrayOf(
                            Section(0, "W trakcie realizacji"),
                            Section(tmpOffersTriple.third.size, "Do rozpatrzenia"),
                            Section(tmpOffersTriple.third.size + tmpOffersTriple.second.size, "Wystawione")
                        )
                        sectionedAdapter.setSections(sections)
                        val combinedOffers = mutableListOf<Offer>()
                        combinedOffers.addAll(tmpOffersTriple.third)
                        combinedOffers.addAll(tmpOffersTriple.second)
                        combinedOffers.addAll(tmpOffersTriple.first)
                        adapter.swapData(combinedOffers)
                        rootView.swipe_refresh_layout.isRefreshing = false
                        sectionedAdapter.notifyDataSetChanged()
                    }

                    override fun onFailure() {
                        Toast.makeText(context,"Pobieranie ofert nie powiodło się",Toast.LENGTH_SHORT).show()
                        rootView.swipe_refresh_layout.isRefreshing = false
                    }
                }
                firebaseCommunicator.getMyOffers(dbListener)

            }
            2 -> {
                val sections = mutableListOf<Section>()
                val allOffersToDo = mutableListOf<Offer>()
                val dbListener = object : IOnGetDataListener {
                    override fun onFailure() {
                        Toast.makeText(context,"Pobieranie ofert nie powiodło się",Toast.LENGTH_SHORT).show()
                        rootView.swipe_refresh_layout.isRefreshing = false
                    }

                    override fun onSuccess(data: Any?) {
                        val offersToExecute = (data as QuerySnapshot).documents.map { v -> v.toObject(Offer::class.java)!! }
                        sections.add(Section(0, "Do realizacji"))
                        allOffersToDo.addAll(offersToExecute)

                        val dbListener2 = object : IOnGetDataListener {
                            override fun onFailure() {
                                Toast.makeText(context, "Pobieranie ofert nie powiodło się", Toast.LENGTH_SHORT).show()
                                rootView.swipe_refresh_layout.isRefreshing = false
                            }

                            override fun onSuccess(data: Any?) {
                                val offersInAccepted = (data as QuerySnapshot).documents.map { v -> v.toObject(Offer::class.java)!! }
                                sections.add(Section(offersToExecute.size, "Oczekujące na akceptację"))
                                allOffersToDo.addAll(offersInAccepted)

                                val dbListener3 = object : IOnGetDataListener {
                                    override fun onFailure() {
                                        Toast.makeText(context, "Pobieranie ofert nie powiodło się", Toast.LENGTH_SHORT).show()
                                        rootView.swipe_refresh_layout.isRefreshing = false
                                    }

                                    override fun onSuccess(data: Any?) {
                                        val offersInContacts = (data as QuerySnapshot).documents.map { v -> v.toObject(Offer::class.java)!! }
                                        sections.add(Section(offersToExecute.size + offersInAccepted.size, "Oczekujące na kontakt"))
                                        allOffersToDo.addAll(offersInContacts)
                                        sectionedAdapter.setSections(sections.toTypedArray())
                                        adapter.swapData(allOffersToDo)
                                        rootView.swipe_refresh_layout.isRefreshing = false
                                        sectionedAdapter.notifyDataSetChanged()
                                    }
                                }
                                firebaseCommunicator.getContactOffers(dbListener3)
                            }
                        }
                        firebaseCommunicator.getAcceptedOffers(dbListener2)
                    }
                }
                firebaseCommunicator.getMyOffersToExecute(dbListener)

            }
        }
    }

    private fun getAllOffers(allOffers : List<Offer>) : List<Offer> {
        val resultOffers : MutableList<Offer> = mutableListOf()
        val myId = firebaseCommunicator.getCurrentId()
        for(offer in allOffers) {
            if(offer.executor == myId || offer.issuer == myId ||
                    offer.contacts.contains(myId) || offer.accepted.contains(myId)) {
                continue
            }
            else {
                resultOffers.add(offer)
            }
        }
        return resultOffers
    }

    private fun getMyOffers(allOffers : List<Offer>) : Triple<List<Offer>, List<Offer>, List<Offer>> {
        val resultOffersA : MutableList<Offer> = mutableListOf()
        val resultOffersB : MutableList<Offer> = mutableListOf()
        val resultOffersC : MutableList<Offer> = mutableListOf()
        for(offer in allOffers) {
            if(offer.contacts.isEmpty() && offer.accepted.isEmpty() && offer.executor.isNullOrEmpty()) {
                resultOffersA.add(offer)
            } else if((!offer.contacts.isEmpty() || !offer.accepted.isEmpty()) && offer.executor.isNullOrEmpty()) {
                resultOffersB.add(offer)
            } else if(!offer.executor.isNullOrEmpty()) {
                resultOffersC.add(offer)
            }
        }
        return Triple(resultOffersA, resultOffersB, resultOffersC)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    override fun onItemSelected(parent: AdapterView<*>, view: View, pos: Int, id: Long) {
        currentList = pos
        populateRightList()
    }

    override fun onNothingSelected(parent: AdapterView<*>) {
        return
    }

    private fun startAddEditOfferActivity() {
        val editIntent = Intent(context, AddEditOffer::class.java)
        startActivityForResult(editIntent, ADD_OFFER)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == ADD_OFFER && resultCode == Activity.RESULT_OK) {
            Toast.makeText(activity, "Dodano ofertę", Toast.LENGTH_SHORT).show()
        }
    }

    companion object {
        @JvmStatic
        fun newInstance(myId: String) =
            OffersListsFragment().apply {
                arguments = Bundle().apply {
                    putString(PROFILE_ID_PARAM, myId)
                }
            }
    }
}
