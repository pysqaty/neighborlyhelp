package com.example.androidclassproject.neighborlyhelp.enums

enum class OfferCategory {
    Pets,
    IT,
    Other,
}