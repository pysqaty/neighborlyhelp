package com.example.androidclassproject.neighborlyhelp.models

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Environment
import android.os.Parcel
import android.os.Parcelable
import java.io.*
import java.util.*


class EditProfileData (
    var nickname: String,
    var firstName: String?,
    var lastName: String?,
    var email: String?,
    var phoneNumber: String?,
    var photoPath: String?
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    )

    constructor() : this("", "", "", "", "", null)

    fun getBitmapFromPath() : Bitmap? {
        var bmp: Bitmap? = null
        try {
            val `is` = FileInputStream(photoPath)
            bmp = BitmapFactory.decodeStream(`is`)
            `is`.close()
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return bmp
    }

    fun saveBitmapUnderPath(bmp: Bitmap, newPhotoPath: String) {
        val path = Environment.getExternalStorageDirectory().toString()
        val file = File(path, newPhotoPath)
        try {
            val stream: OutputStream = FileOutputStream(file)
            bmp.compress(Bitmap.CompressFormat.JPEG, 20, stream)
            stream.flush()
            stream.close()
        } catch (e: IOException){
            e.printStackTrace()
        }

        bmp.recycle()
        photoPath = file.absolutePath
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(nickname)
        parcel.writeString(firstName)
        parcel.writeString(lastName)
        parcel.writeString(email)
        parcel.writeString(phoneNumber)
        parcel.writeString(photoPath)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<EditProfileData> {
        override fun createFromParcel(parcel: Parcel): EditProfileData {
            return EditProfileData(parcel)
        }

        override fun newArray(size: Int): Array<EditProfileData?> {
            return arrayOfNulls(size)
        }
    }
}