// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');

// The Firebase Admin SDK to access the Firebase Realtime Database.
const admin = require('firebase-admin');
admin.initializeApp();

exports.checkAndDeleteOffer = functions.firestore
    .document('offers/{offerId}')
    .onUpdate((change, context) => {

      const newValue = change.after.data();
      const previousValue = change.before.data();

      if(newValue.wasRatedByExecutor == previousValue.wasRatedByExecutor
        && newValue.wasRatedByIssuer == previousValue.wasRatedByIssuer) {
            return null
        }

      if(previousValue.wasRatedByExecutor == true) {
          newValue.wasRatedByExecutor = true
      }

      if(previousValue.wasRatedByIssuer == true) {
        newValue.wasRatedByIssuer = true
      }

      if(newValue.wasRatedByExecutor == true
        && newValue.wasRatedByIssuer == true) {
            var prefix = "offers/"
            admin.firestore().collection('offers').doc(newValue.id).delete()
        }
        return null
    });

exports.calculateUserRating = functions.firestore
    .document('users/{userId}')
    .onUpdate((change, context) => {
        
        const newValue = change.after.data();
        const previousValue = change.before.data();
        if(previousValue.ratesCounter == newValue.ratesCounter) {
            return null
        }
        var prevSum = previousValue.rating * previousValue.ratesCounter
        var currentSum = prevSum + newValue.rating
        var newRating = (currentSum) / (previousValue.ratesCounter + 1)
        return change.after.ref.set({
            rating: newRating
          }, {merge: true});
    })